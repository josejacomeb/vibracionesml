#include <iostream>
#include <math.h>
#include <chrono>
#include <ctime>
#include <unistd.h>
#include <fstream>
#include <random>
#include <algorithm>
#include <eigen3/unsupported/Eigen/FFT>
#include <H5Cpp.h>
#include <H5File.h>
#include <stdlib.h>

using namespace std;
using namespace H5;

float amplitud = 3;
float frecuencia = 60; //[Hz]
float frecuenciamuestreo = 1200;//[Khz]
int totalnumeromuestras = 300;
int numerodepuntospormuestra = 1000;
string nombrearchivo = "";
int tipoerror = 0; //Error 0, muestra sin erros, error 1 falla electrica, 2 barras rotas, 3
int Ilsb = 20, Iusb = 20;
int s = 0.333;
int wr = 40;
size_t tamanhovector = 0;
bool seguir = false, generarcsv = true;


void ayuda(){
    cout << "" << "Escribir generador de ondas y la función" <<  endl;
    cout << "--a Especificar el valor de la amplitud de la onda" << endl;
    cout << "--fm Especificar el valor de la frecuencia de muestreo[Hz]" << endl;
    cout << "--t Especificar el numero total de muestras(maximo 1000)" << endl;
    cout << "--n Especificar el numero de puntos por muestra" << endl;
    cout << "--f Especificar el nombre del archivo de salida" << endl;
    cout << "--e Especificar el tipo de error" << endl;
    cout << "-sincomprobacion No comprobar los datos y ejecutar el programa" << endl;
    cout << "-sincsv Parar la generacion de archivos csv" << endl;
}

int main(int argc, char* argv[]){
    cout << "****************************" << endl;
    cout << "Generador de ondas para MCSA" << endl;
    cout << "****************************" << endl;


    cout << argc << endl;
    if(argc < 3){
        ayuda();
        return 1;
    }

    for(int i = 0; i < argc; i++){

        if(std::string(argv[i]) == "--a"){
            if(i + 1 < argc){
                amplitud = atof(argv[i+1]);
                cout << "Cambiado el valor de amplitud a: " << amplitud << endl;
            }
            else{
                cout << "Indique el valor despues del argumento amplitud" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "--fm"){
            if(i + 1 < argc){
                frecuenciamuestreo = atof(argv[i+1]);
                cout << "Cambiado el valor de frecuencia de muestreo a: " << frecuenciamuestreo << endl;
            }
            else{
                cout << "Indique el valor despues del argumento frecuencia de muestreo" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "--t"){
            if(i + 1 < argc){
                totalnumeromuestras = atoi(argv[i+1]);
                cout << "Cambiado el valor de numero de muestras total a: " << totalnumeromuestras << endl;
            }
            else{
                cout << "Indique el valor despues del argumento numero de muestras total" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "--n"){
            if(i + 1 < argc){
                numerodepuntospormuestra = atoi(argv[i+1]);
                cout << "Cambiado el valor de numero de muestras por ciclo a: " << numerodepuntospormuestra  << endl;
            }
            else{
                cout << "Indique el valor despues del argumento numero de muestras por ciclo" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "--f"){
            cout << "Cambiando --f" << endl;
            if(i + 1 < argc){
                nombrearchivo = string(argv[i+1]);
                cout << "Cambiado el valor de nombre de archivo a: " << nombrearchivo << endl;
            }
            else{
                cout << "Indique el valor despues del nombre de archivo" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "--e"){
            if(i + 1 <= argc){
                tipoerror = atoi(argv[i+1]);
                if(tipoerror < 0 && tipoerror > 3){
                    cout << "Especifique un tipo de error válido" << endl;
                    ayuda();
                    return -1;
                }
                cout << "Cambiado el valor de tipo de error a: " << tipoerror << endl;
            }
            else{
                cout << "Indique el valor despues del tipo de error" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "-sincomprobacion"){
            cout << "Generar automaticamente las muestras" << endl;
            seguir = true;
        }
        else if(std::string(argv[i]) == "-sincsv"){
            generarcsv = false;
            cout << "No generar csv" << endl;
        }
    }
    if(nombrearchivo == "") {
        cout << "Especifique un nombre de archivo con --f" << endl;
        return -1;
    }
    cout << "******************************************" << endl;
    cout << "Datos para el Generador de ondas para MCSA" << endl;
    cout << "******************************************" << endl;
    cout << "Amplitud corriente:  " << amplitud  << endl;
    cout << "Frecuencia:          " << frecuencia << endl; //[Hz]
    cout << "Frecuencia muestro:  " << frecuenciamuestreo << endl;//[Hz]
    cout << "Total muestras:      " << totalnumeromuestras << endl;
    cout << "Puntos por muestra:  " << numerodepuntospormuestra << endl;
    cout << "Nombre archivo:      " << nombrearchivo << endl;
    cout << "Tipo error:          " << tipoerror << endl; //Error 0, muestra sin erros, error 1 falla electrica, 2 barras rotas, 3
    char opcion;
    if(!seguir){
        cout << "¿Continuar?[S/N]: "; cin >> opcion;
        if(opcion == 'N' || opcion == 'n'){
            cout << "Programa finalizado" << endl;
            return 1;
        }
    }
    //Formato, primera columna el label, las demás los datos
    //Primera etapa, obtener los puntos
    std::vector<std::vector<float>> muestras;
    std::vector<float> muestra;
    const double mean = 0.0;
    const double stddev = amplitud/10;
    std::default_random_engine generator;
    std::normal_distribution<double> dist(mean, stddev); //Generador de ruido blanco
    cout << "Generando muestras" << endl;
    float tiempomuestreo = 1/frecuenciamuestreo;
    cout << "tiempo de muestreo: " << tiempomuestreo << endl;
    float argumento  = 0;


    for(int i = 0; i < totalnumeromuestras; i++){
        argumento = (rand() % 200 - 100)/100.0;
        muestra.clear();
        for(int j = 0; j < numerodepuntospormuestra; j++){
            muestra.push_back(amplitud*cos(frecuencia*argumento) + dist(generator));//Calculo del seno y añadir ruido blanco
            argumento += tiempomuestreo;
        }
        muestras.push_back(muestra);
    }
    cout << "Finalizo de generar muestras" << endl;
    time_t rawtime;
    struct tm * timeinfo;
    string tiempo = "";
    ofstream miarchivo, archivofunciontiempo;
    cout << "Guardando archivo" << endl;
    if(generarcsv){
        archivofunciontiempo.open("muestra"+nombrearchivo+".csv");
        for(size_t i = 0; i < muestras.at(muestras.size() - 1).size(); i++){
            time (&rawtime);
            timeinfo = localtime (&rawtime);
            tiempo = asctime(timeinfo);
            archivofunciontiempo  << muestras.at(muestras.size() - 1).at(i) << "," << tiempo;
            usleep(tiempomuestreo*1000000);

        }
        archivofunciontiempo.close();
    }
    tamanhovector = muestras.size();

    cout << "Introduciendo error por barras rotas" << endl;
    //introducir falla por barra rota
    for(size_t i = 0; i < tamanhovector; i++){
        argumento = (rand() % 200 - 100)/100.0;
        muestra.clear();
        for(size_t j = 0; j < muestras.at(i).size(); j++){
            muestra.push_back(muestras.at(i).at(j) + Ilsb*cos((1-2*s)*frecuencia*tiempomuestreo)+Iusb*cos((1+2*s)*frecuencia*tiempomuestreo));
            argumento += tiempomuestreo;
        }
        muestras.push_back(muestra);
    }
    cout << "Finalizo de generar muestras" << endl;
    if(generarcsv){
        archivofunciontiempo.open("muestrabarrasrotas" + nombrearchivo +".csv");
        for(size_t i = 0; i < muestras.at(muestras.size() - 1).size(); i++){
            time (&rawtime);
            timeinfo = localtime (&rawtime);
            tiempo = asctime(timeinfo);
            archivofunciontiempo  << muestras.at(muestras.size() - 1).at(i) << "," << tiempo;
            usleep(tiempomuestreo*1000000);

        }
        archivofunciontiempo.close();
    }

    //introducir falla por asimetria del eje
    cout << "Generando muestras de asimetria del eje" << endl;
    for(size_t i = 0; i < tamanhovector; i++){
        argumento = (rand() % 200 - 100)/100.0;
        muestra.clear();
        for(size_t j = 0; j < muestras.at(i).size(); j++){
            muestra.push_back(muestras.at(i).at(j) + Ilsb*cos((frecuencia - wr)*tiempomuestreo)+Iusb*cos((frecuencia + wr)*frecuencia*tiempomuestreo));
            argumento += tiempomuestreo;
        }
        muestras.push_back(muestra);
    }
    if(generarcsv){
        archivofunciontiempo.open("muestraasimetria" + nombrearchivo + ".csv");
        for(size_t i = 0; i < muestras.at(muestras.size() - 1).size(); i++){
            time (&rawtime);
            timeinfo = localtime (&rawtime);
            tiempo = asctime(timeinfo);
            archivofunciontiempo  << muestras.at(muestras.size() - 1).at(i) << "," << tiempo;
            usleep(tiempomuestreo*1000000);

        }
        archivofunciontiempo.close();
    }
    cout << "Convertir a FFT" << endl;
    //Convertir a FFT
    Eigen::FFT<float> fft;
    std::vector<std::vector<std::complex<float>>> muestrasFFT;
    std::vector<std::complex<float> > muestraFFT;

    for(size_t i = 0; i < muestras.size(); i++){
        muestra.clear();
        muestra = muestras.at(i);
        fft.fwd(muestraFFT, muestra);
        muestrasFFT.push_back(muestraFFT);
    }
    //Convertir a dB
    cout << "Convertir a dB" << endl;
    std::vector<std::vector<float>> muestrasdB;
    std::vector<float> muestradB;
    for(size_t i = 0; i < muestrasFFT.size(); i++){
        muestradB.clear();
        for(size_t j = 0; j < muestrasFFT.at(i).size(); j++){
            muestradB.push_back(20*log(std::abs(muestrasFFT.at(i).at(j))/muestrasFFT.at(i).size()));
        }
        muestrasdB.push_back(muestradB);
    }

    //Normalizacion y escalado a -1, 1
    cout << "Normalizacion y escalado" << endl;

    std::vector<std::vector<float>> muestrasNormalizada;


    for(size_t i = 0; i < muestrasdB.size(); i++){
        muestradB.clear();
        muestradB = muestrasdB.at(i);
        float maximo = -10000000000000;
        float minimo = 0;
        float promedio = accumulate( muestradB.begin(), muestradB.end(), 0.0)/muestradB.size();
        for(size_t j = 0; j < muestradB.size(); j++){
            if(maximo < muestradB.at(j)) maximo = muestradB.at(j);
            if(minimo > muestradB.at(j)) minimo = muestradB.at(j);
        }
        float limiteinferior = -1, limitesuperior = 1;;
        for(size_t j = 0; j < muestradB.size(); j++){
            muestradB.at(j) = limiteinferior + ((muestradB.at(j) - minimo)*(limitesuperior - limiteinferior))/(maximo - minimo);
        }
        muestrasNormalizada.push_back(muestradB);
    }
    cout << "Generando archivo final de muestras" << endl;
    // Open an existing file and dataset.
    //Generacion Dataset
    const H5std_string FILE_NAME(nombrearchivo + ".h5");
    const H5std_string DATASET_NAME1("data");
    const H5std_string DATASET_NAME2("label");
    const int	RANK = 4;
    H5File file(FILE_NAME, H5F_ACC_TRUNC);
    hsize_t dims[RANK];               // dataset dimensions
    dims[0] = muestrasNormalizada.size();
    dims[1] = 1;
    dims[2] = 1;
    dims[3] = muestrasNormalizada.at(0).size();

    cout << "dims[0]" << muestrasNormalizada.size() << endl;
    cout << "dims[1]" << muestrasNormalizada.at(0).size() << endl;
    DataSpace dataspace(RANK, dims);

    DataType datatype(H5::PredType::NATIVE_DOUBLE);
    DataSet dataset = file.createDataSet(DATASET_NAME1, datatype, dataspace);
    cout << "Generando HF5" << endl;
    double arrayMuestrasnormalizada[dims[0]][dims[1]][dims[2]][dims[3]];
    for(unsigned long i = 0; i < muestrasNormalizada.size(); i++){
        for(size_t j = 0; j < muestrasNormalizada.at(i).size(); j++){
            arrayMuestrasnormalizada[i][0][0][j] = muestrasNormalizada.at(i).at(j);
        }
    }
    dataset.write(arrayMuestrasnormalizada, PredType::NATIVE_DOUBLE);
    dataset.close();
    dataspace.close();
    const int RANKcategorias = 4;
    hsize_t dimsCat[RANKcategorias];
    dimsCat[0] = muestrasNormalizada.size();
    dimsCat[1] = 1;
    dimsCat[2] = 1;
    dimsCat[3] = 1;
    DataSpace dataspaceCat(RANKcategorias, dimsCat);
    DataType datatypeCat(H5::PredType::NATIVE_DOUBLE);
    DataSet datasetCat = file.createDataSet(DATASET_NAME2, datatypeCat, dataspaceCat);
    double categoriasMuestrasnormalizada[dimsCat[0]][dimsCat[1]][dimsCat[2]][dimsCat[3]];
    for(unsigned long i = 0; i < muestrasNormalizada.size(); i++){
        if(i < tamanhovector) tipoerror = 0;
        else if((i >= tamanhovector) && (i < 2*tamanhovector)) tipoerror = 1;
        else tipoerror = 2;
        categoriasMuestrasnormalizada[i][0][0][0] = int(tipoerror);
        std::cout << "Tipo error: " <<  tipoerror << " Tipo error vector: " << categoriasMuestrasnormalizada[i][0][0][0]  <<  std::endl;
    }

    datasetCat.write(categoriasMuestrasnormalizada, PredType::NATIVE_DOUBLE);

    datasetCat.close();
    dataspaceCat.close();
    file.close();
    if(generarcsv){
        miarchivo.open (nombrearchivo+".csv");
        for(size_t i = 0; i < muestrasNormalizada.size(); i++){
            if(i < tamanhovector) tipoerror = 0;
            else if((i >= tamanhovector) && (i < 2*tamanhovector)) tipoerror = 1;
            else tipoerror = 2;

            string linea = to_string(tipoerror) + ",";
            for(size_t j = 0; j < muestrasNormalizada.at(i).size(); j++){
                linea+=to_string(muestrasNormalizada.at(i).at(j));
                if(j < muestrasNormalizada.at(i).size() - 1) linea+=",";
            }
            miarchivo  << linea << endl;
        }
        cout << "Archivo guardado como: " << nombrearchivo << ".csv" << endl;
        miarchivo.close();
    }
    return 0;
}
