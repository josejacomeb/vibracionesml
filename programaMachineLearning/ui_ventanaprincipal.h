/********************************************************************************
** Form generated from reading UI file 'ventanaprincipal.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAPRINCIPAL_H
#define UI_VENTANAPRINCIPAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_VentanaPrincipal
{
public:
    QWidget *centralWidget;
    QCustomPlot *graficoTiempo;
    QCustomPlot *graficoFrecuencia;
    QLabel *label;
    QLabel *label_2;
    QPushButton *botonAbrir;
    QLabel *rutaArchivo;
    QLabel *label_3;
    QCustomPlot *graficoEntradaRed;
    QLabel *salidaClasificacion;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *VentanaPrincipal)
    {
        if (VentanaPrincipal->objectName().isEmpty())
            VentanaPrincipal->setObjectName(QStringLiteral("VentanaPrincipal"));
        VentanaPrincipal->resize(1024, 720);
        centralWidget = new QWidget(VentanaPrincipal);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        graficoTiempo = new QCustomPlot(centralWidget);
        graficoTiempo->setObjectName(QStringLiteral("graficoTiempo"));
        graficoTiempo->setGeometry(QRect(20, 60, 501, 211));
        graficoFrecuencia = new QCustomPlot(centralWidget);
        graficoFrecuencia->setObjectName(QStringLiteral("graficoFrecuencia"));
        graficoFrecuencia->setGeometry(QRect(20, 290, 481, 211));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 40, 131, 18));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 270, 181, 18));
        botonAbrir = new QPushButton(centralWidget);
        botonAbrir->setObjectName(QStringLiteral("botonAbrir"));
        botonAbrir->setGeometry(QRect(910, 0, 111, 34));
        rutaArchivo = new QLabel(centralWidget);
        rutaArchivo->setObjectName(QStringLiteral("rutaArchivo"));
        rutaArchivo->setGeometry(QRect(30, 10, 871, 18));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 500, 181, 18));
        graficoEntradaRed = new QCustomPlot(centralWidget);
        graficoEntradaRed->setObjectName(QStringLiteral("graficoEntradaRed"));
        graficoEntradaRed->setGeometry(QRect(20, 520, 481, 121));
        salidaClasificacion = new QLabel(centralWidget);
        salidaClasificacion->setObjectName(QStringLiteral("salidaClasificacion"));
        salidaClasificacion->setGeometry(QRect(530, 540, 441, 20));
        VentanaPrincipal->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(VentanaPrincipal);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 32));
        VentanaPrincipal->setMenuBar(menuBar);
        mainToolBar = new QToolBar(VentanaPrincipal);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        VentanaPrincipal->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(VentanaPrincipal);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        VentanaPrincipal->setStatusBar(statusBar);

        retranslateUi(VentanaPrincipal);

        QMetaObject::connectSlotsByName(VentanaPrincipal);
    } // setupUi

    void retranslateUi(QMainWindow *VentanaPrincipal)
    {
        VentanaPrincipal->setWindowTitle(QApplication::translate("VentanaPrincipal", "MCSA Deep Learning", Q_NULLPTR));
        label->setText(QApplication::translate("VentanaPrincipal", "Dominio del tiempo", Q_NULLPTR));
        label_2->setText(QApplication::translate("VentanaPrincipal", "Dominio de la frecuencia", Q_NULLPTR));
        botonAbrir->setText(QApplication::translate("VentanaPrincipal", "Abrir Archivo", Q_NULLPTR));
        rutaArchivo->setText(QString());
        label_3->setText(QApplication::translate("VentanaPrincipal", "Entrada Red", Q_NULLPTR));
        salidaClasificacion->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class VentanaPrincipal: public Ui_VentanaPrincipal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAPRINCIPAL_H
