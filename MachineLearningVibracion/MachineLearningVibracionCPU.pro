#-------------------------------------------------
#
# Project created by QtCreator 2019-09-03T23:17:12
#
#-------------------------------------------------

QT       += core gui
CONFIG +=  c++14
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = MachineLearningVibracion
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        ventanaprincipal.cpp \
    qcustomplot.cpp \

HEADERS += \
    ventanaprincipal.h \
    qcustomplot.h \
    ventanaprincipal.ui \

SUBDIRS += \
    MachineLearningVibracion.pro

unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lcaffe -lglog

INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu

FORMS += \
    ventanaprincipal.ui

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += eigen3

unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lboost_math_tr1

INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu


unix:!macx: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lboost_system

INCLUDEPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu
