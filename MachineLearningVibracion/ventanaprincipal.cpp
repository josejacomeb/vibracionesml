#include "ventanaprincipal.h"
#include "ui_ventanaprincipal.h"

VentanaPrincipal::VentanaPrincipal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VentanaPrincipal)
{
    ui->setupUi(this);
#ifdef CPU_ONLY
    caffe::Caffe::set_mode(caffe::Caffe::CPU);
#else
    caffe::Caffe::set_mode(caffe::Caffe::GPU);
#endif
    red_.reset(new caffe::Net<float>("../ModeloCaffe/arquitecturaredMCSA.prototxt", caffe::TEST));
    red_->CopyTrainedLayersFrom("../ModeloCaffe/redMCSAmodelo.caffemodel");
    numerodatosentrada = red_->input_blobs()[0]->shape(3);
    nombresdesdearchivo.open("../ModeloCaffe/nombres.txt");

    while (std::getline(nombresdesdearchivo, linea)){
        nombres.push_back(std::string(linea));
        qDebug() << QString::fromStdString(linea);
    }
    ui->graficoTiempo->addGraph();
    ui->graficoTiempo->xAxis->setLabel("Tiempo");
    ui->graficoTiempo->yAxis->setLabel("Valor");
    ui->graficoEntradaRed->addGraph();
    ui->graficoEntradaRed->yAxis->setRange(-1,1);
    ui->graficoFrecuencia->addGraph();
    ui->graficoFrecuencia->xAxis->setLabel("Frecuencia");
    ui->graficoFrecuencia->yAxis->setLabel("Módulo");
}

VentanaPrincipal::~VentanaPrincipal()
{
    delete ui;
}

void VentanaPrincipal::on_botonAbrir_clicked()
{
    QString ruta =  QFileDialog::getOpenFileName(this, "Abrir el archivo que contiene los datos de vibración", QDir::homePath(), "Archivo CSV (*.csv)");
    ui->rutaArchivo->setText(ruta);

    QFile archivocsv(ruta);
    ui->graficoTiempo->setLocale(QLocale(QLocale::Spanish, QLocale::Ecuador));
    std::vector<float> vectortiempo;
    std::vector<std::complex<float> > vectorfrecuencia;
    float frecuencia = 0;
    double valormenory = 1000000, valormayory = 0;
    valoresGrafico.clear();
    valoresGraficoFrecuencia.clear();
    datosEntradaRed.clear();
    if (archivocsv.open(QIODevice::ReadOnly))
    {
        QTextStream in(&archivocsv);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            valorGraficounico.value = line.split(',').first().toDouble();
            QDateTime tiempo = QDateTime::fromString(line.split(',').back(),Qt::TextDate);
            valorGraficounico.key = vectortiempo.size(); //tiempo.toMSecsSinceEpoch();
            if(valorGraficounico.value < valormenory) valormenory = valorGraficounico.value;
            if(valorGraficounico.value > valormayory) valormayory = valorGraficounico.value;
            vectortiempo.push_back(valorGraficounico.value);
            valoresGrafico.append(valorGraficounico);
        }
        archivocsv.close();
    }
    float tiempo = (valoresGrafico.last().key - valoresGrafico.first().key)/1000;
    frecuencia = 1 /(tiempo/valoresGrafico.size()) ;
    Eigen::FFT<float> fft;
    fft.fwd(vectorfrecuencia, vectortiempo);
    double valormenorx = 1000000, valormayorx = 0;
    for(size_t i = 0; i < vectorfrecuencia.size(); i++){
        valorGraficoFrecuenciaunico.key = i*frecuencia/vectorfrecuencia.size();
        valorGraficoFrecuenciaunico.value = 20*log(std::abs(vectorfrecuencia.at(i))/vectorfrecuencia.size()); //Change to power spectrum values
        if(valorGraficoFrecuenciaunico.value < valormenorx) valormenorx = valorGraficoFrecuenciaunico.value;
        if(valorGraficoFrecuenciaunico.value > valormayorx) valormayorx = valorGraficoFrecuenciaunico.value;
        valoresGraficoFrecuencia.append(valorGraficoFrecuenciaunico);
    }
    // configure bottom axis to show date instead of number:
    //QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    //dateTicker->setDateTimeFormat("d. MMMM\nyyyy");

    //ui->graficoTiempo->xAxis->setTicker(dateTicker);
    // create graph and assign data to it:

    ui->graficoTiempo->graph()->data()->set(valoresGrafico);
    ui->graficoTiempo->xAxis->setRange(valoresGrafico.first().key, valoresGrafico.last().key);
    ui->graficoTiempo->yAxis->setRange(valormenory - abs(valormenory - valormayory)/50.0, valormayory + abs(valormenory - valormayory)/10.0);
    ui->graficoTiempo->replot();

    // set axes ranges, so we see all data:

    ui->graficoFrecuencia->graph()->data()->set(valoresGraficoFrecuencia);
    ui->graficoFrecuencia->xAxis->setRange(valoresGraficoFrecuencia.first().key - abs(valoresGraficoFrecuencia.first().key - valoresGraficoFrecuencia.last().key)/10.0 , valoresGraficoFrecuencia.last().key + abs(valoresGraficoFrecuencia.first().key - valoresGraficoFrecuencia.last().key)/10.0);
    ui->graficoFrecuencia->yAxis->setRange(valormenorx - abs(valormenorx - valormayorx)/50.0, valormayorx + abs(valormenorx - valormayorx)/10.0);
    ui->graficoFrecuencia->replot();
    //Añadir entrada de red
    bool calcular = true;
    if(vectorfrecuencia.size() > size_t(numerodatosentrada)){
        qDebug() << "Muy grande" << endl;
        //QMessageBox::StandardButton aceptar = QMessageBox::Information(this, "Información","Vector muy grande que al de entrada de red, reduciendo vector a " + QString::number(numerodatosentrada) + " elementos");
    }
    else if(vectorfrecuencia.size() < size_t(numerodatosentrada)){
        //QMessageBox::StandardButton aceptar = QMessageBox::Information(this, "Información","Vector muy pequeño, no se puede calcular, ingrese otro archivo");
        calcular = false;
    }
    if(calcular){
        red_->ClearParamDiffs();
        caffe::Blob<float> *blob_entrada = red_->input_blobs()[0];
        float* vectorEntradaRed = blob_entrada->mutable_cpu_data();

        float maximo = -10000000000000;
        float minimo = 0;
        for(size_t j = 0; j < size_t(valoresGraficoFrecuencia.size()); j++){
            if(maximo < valoresGraficoFrecuencia.at(j).value) maximo = valoresGraficoFrecuencia.at(j).value;
            if(minimo > valoresGraficoFrecuencia.at(j).value) minimo = valoresGraficoFrecuencia.at(j).value;
        }
        //Normalizar valores
        float limiteinferior = -1, limitesuperior = 1;
        for(size_t j = 0; j < size_t(valoresGraficoFrecuencia.size()); j++){
            datoEntradaRed.value = limiteinferior + ((valoresGraficoFrecuencia.at(j).value - minimo)*(limitesuperior - limiteinferior))/(maximo - minimo);
            datoEntradaRed.key = j;
            datosEntradaRed.push_back(datoEntradaRed);
            vectorEntradaRed[j] = datoEntradaRed.value;
        }
        std::vector<float> vectorEntradaRedStd{vectorEntradaRed, vectorEntradaRed +  valoresGraficoFrecuencia.size() };
        acumulador = std::for_each( vectorEntradaRedStd.begin(), vectorEntradaRedStd.end(), acumulador) ;
        /*qDebug() << "Media: " << mean(acumulador);
        qDebug() << "Kurtois: " << kurtosis(acumulador);
        qDebug() << "Desviación estándar: " <<sqrt(variance(acumulador));
        qDebug() << "Varianza: " << variance(acumulador);
        qDebug() << "Skewness: " << skewness(acumulador);
        qDebug() << "Minimo: " << min(acumulador) << endl;
        qDebug() << "Maximo: " << max(acumulador) << endl;
        qDebug() << "RMS: " << sqrt(abs(sum(acumulador))/count(acumulador)) << endl;
        */

        ui->graficoEntradaRed->xAxis->setRange(0,numerodatosentrada);
        ui->graficoEntradaRed->graph()->data()->set(datosEntradaRed);
        ui->graficoEntradaRed->replot();
        red_->Forward();
        boost::shared_ptr<caffe::Blob<float> > capa_salida = red_->blob_by_name("prob");
        const float* begin = capa_salida->cpu_data();
        const float* end = begin + capa_salida->channels();

        std::vector<float> resultados(begin, end);
        //Código red
        paresclasificacion.clear();
        for(size_t i = 0; i < resultados.size(); i++){
            paresclasificacion.push_back(std::make_pair(resultados.at(i),nombres.at(i)));
        }
        for(size_t j = 0; j < paresclasificacion.size(); j++){
            qDebug() << "Salida: Probabilidad: " << paresclasificacion.at(j).first << " Clase: " << QString::fromStdString(paresclasificacion.at(j).second);
        }
        std::sort(paresclasificacion.begin(),paresclasificacion.end(), [](const std::pair<float, std::string> a, const std::pair<float, std::string> b) {
            return a.first > b.first;
        } );
        ui->salidaClasificacion->setText(QString("Es ") + QString::fromStdString(paresclasificacion.at(0).second) + " con un " + QString::number(paresclasificacion.at(0).first*100) + " de probabilidad" );
        qDebug() << "Salida: Probabilidad: " << paresclasificacion.at(0).first << " Clase: " << QString::fromStdString(paresclasificacion.at(0).second);
        QProcess p;
        QStringList params;
        params << "MCSAAnfisTest.py";
        params << "--de";
        params << QString::number(sqrt(variance(acumulador)));
        ui->de->setText(QString::number(sqrt(variance(acumulador))));
        params << "--ku";
        params << QString::number(kurtosis(acumulador));
        ui->ku->setText(QString::number(kurtosis(acumulador)));
        params << "--me";
        params << QString::number(mean(acumulador));
        ui->me->setText(QString::number(mean(acumulador)));
        params << "--rms";
        params << QString::number(sqrt(abs(sum(acumulador))/count(acumulador)));
        ui->rms->setText(QString::number(sqrt(abs(sum(acumulador))/count(acumulador))));
        params << "--sk";
        params << QString::number(skewness(acumulador));
        ui->sk->setText(QString::number(skewness(acumulador)));
        params << "--va";
        params << QString::number(variance(acumulador));
        ui->va->setText(QString::number(variance(acumulador)));
        //params << "-W ignore";  //Ignorar los mensajes de Alerta
        qDebug() <<  "Params: " << params << endl;
        p.setWorkingDirectory("./../ModeloANFIS/TensorANFIS/");
        p.start("python3", params);
        while(!p.waitForFinished());
        QByteArray salida_Neurofuzzy = p.readAllStandardOutput();
        QString resultado = "";
        bool empezar_leer = false;
        for(int index = 0; index < salida_Neurofuzzy.size(); index++){
            if(empezar_leer){
                resultado+= QString(salida_Neurofuzzy.at(index));
                break;
            }
            if(salida_Neurofuzzy.at(index)=='{')
                empezar_leer = true;
            else if(salida_Neurofuzzy.at(index)=='}')
                break;
        }
        qDebug() << "ANFIS Resultado: " <<resultado << endl;
        qDebug() << QString::fromUtf8(nombres.at(resultado.toInt()).c_str()) << endl;
        ui->salidaNeurofuzzy->setText(QString::fromUtf8(nombres.at(resultado.toInt()).c_str()));
    }
}
