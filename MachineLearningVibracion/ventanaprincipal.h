#ifndef VENTANAPRINCIPAL_H
#define VENTANAPRINCIPAL_H

#include <QMainWindow>
#include <QFile>
#include <QFileDialog> //Para crear los dialogos de obtener datos
#include <QDir> //Obtiene las direcciones absolutas de la carpeta principal
#include <QFile> //Libreria para obtener archivos de datos
#include <qcustomplot.h>
#include <eigen3/unsupported/Eigen/FFT> //Transformada rápida de Fourier
#define CPU_ONLY 1
#include <caffe/caffe.hpp> //Incluye Caffe, libreria de Deep Learning
#include <caffe/layers/memory_data_layer.hpp>
#include <string>
#include <boost/math/distributions.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/kurtosis.hpp>
#include <boost/accumulators/statistics/skewness.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>

namespace Ui {
class VentanaPrincipal;
}

class VentanaPrincipal : public QMainWindow
{
    Q_OBJECT

public:
    explicit VentanaPrincipal(QWidget *parent = 0);
    ~VentanaPrincipal();
    QVector<QCPGraphData> valoresGrafico, datosEntradaRed, valoresGraficoFrecuencia;
    QCPGraphData valorGraficounico,valorGraficoFrecuenciaunico, datoEntradaRed;
    std::vector<std::string> nombres;

private slots:
    void on_botonAbrir_clicked();

private:
    Ui::VentanaPrincipal *ui;
    std::shared_ptr<caffe::Net<float> > red_;
    int numerodatosentrada = 512;
    std::ifstream nombresdesdearchivo;
    std::string linea;
    std::vector<std::pair<float, std::string> > paresclasificacion;
    boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::min, boost::accumulators::tag::max, boost::accumulators::tag::sum, boost::accumulators::tag::count, boost::accumulators::tag::mean, boost::accumulators::tag::kurtosis, boost::accumulators::tag::variance, boost::accumulators::tag::skewness>> acumulador; //Sirve para obtener las características de de la
};
using namespace boost::accumulators;

#endif // VENTANAPRINCIPAL_H
