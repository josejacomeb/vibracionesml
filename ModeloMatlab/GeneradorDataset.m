clear;
clc;
parametros = Simulink.Mask.get('MotorInduccionFallasRegulador/Motor inducción');
barrasRotas = parametros.Parameters(15); %1
espiras = parametros.Parameters(16); %2
checkfalloAnilloExterior = parametros.Parameters(17); %3
checkfalloAnilloInterior = parametros.Parameters(18); %4
checkfalloBolas = parametros.Parameters(19); %5
checkfalloJaula = parametros.Parameters(20); %6
porcentajeanilloexterior = parametros.Parameters(24); %7
porcentajeanillointerior = parametros.Parameters(25); %8
porcentajefalloBolas = parametros.Parameters(26); %9
porcentajefalloJaula = parametros.Parameters(27); %10
vectorerrores = [barrasRotas espiras checkfalloAnilloExterior porcentajeanilloexterior checkfalloAnilloInterior porcentajeanillointerior checkfalloBolas porcentajefalloBolas checkfalloJaula porcentajefalloJaula];
tiempomuestreo = 1/5120; %5.12Khz
numeropuntos = 512;
cortescada = tiempomuestreo*numeropuntos;
tiempoinicionda = 1;
maximomuestras = 10;
disp("********Generar Dataset*****");
numeromuestras = 5;
disp("**************Generando datos caso normal **************")
maximofallo = 1/100; %El maximo fallo por cada caso
errorinferior = 0.8;
errorsuperior = 1;
erroresaleatorios = [0; 0.958; 0; 0; 0; 0];
limiteinferior = -1;
limitesuperior = 1;
salida = [];
totalmuestras = [];
totallabels = [];
%Muestra normal
label = 0; %Error label
muestras = 0;
estado = 'normal';
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
    disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    erroresaleatorios = (maximofallo)*rand(6,1) - maximofallo/2;
    erroresaleatorios(2) = (rand()*maximofallo)*0.042+0.958;
end
%Muestra Fallo Barras Rotas
label = label + 1; %Error label
muestras = 0;
estado = 'falloBarrasRotas';
erroresaleatorios = [1; 0.958; 0; 0; 0; 0];
disp("**************Generando error barras rotas**************")
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
    disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    erroresaleatorios = (2*maximofallo)*rand(6,1) - maximofallo;
    erroresaleatorios(1) = rand(1)*(errorsuperior - errorinferior) + errorinferior;
    erroresaleatorios(2) = (rand()*maximofallo)*0.042+0.958;
end
%{
%Muestra Fallo Espiras
label = label + 1; %Error label
muestras = 0;
estado = 'falloEspiras';
erroresaleatorios = [0; 1; 0; 0; 0; 0];
disp("**************Generando error fallo espiras**************")
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
    disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    disp(erroresaleatorios);
    erroresaleatorios = (2*maximofallo)*rand(6,1) - maximofallo;
    erroresaleatorios(2) = (rand(1)*(errorsuperior -errorinferior)+errorinferior)*0.042+0.958;
end
%}
%Muestra Fallo Anillo Interior
label = label + 1; %Error label
muestras = 0;
estado = 'falloAnilloInterior';
erroresaleatorios = [0; 0.958; 1; 0; 0; 0];
disp("**************Generando error Anillo Interior**************")
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
     disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    erroresaleatorios = (2*maximofallo)*rand(6,1) - maximofallo;
    erroresaleatorios(3) = rand(1)*(errorsuperior - errorinferior) + errorinferior;
    erroresaleatorios(2) = (rand(1)*(errorsuperior -errorinferior)+errorinferior)*0.042+0.958;
end
%Muestra Fallo Anillo Exterior
label = label + 1; %Error label
muestras = 0;
estado = 'falloAnilloExterior';
erroresaleatorios = [0; 0.958; 0; 1; 0; 0];
disp("**************Generando error Anillo Exterior**************")
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
    disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    disp(erroresaleatorios);
    erroresaleatorios = (2*maximofallo)*rand(6,1) - maximofallo;
    erroresaleatorios(4) = rand(1)*(errorsuperior - errorinferior) + errorinferior;;
    erroresaleatorios(2) = (rand(1)*(errorsuperior -errorinferior)+errorinferior)*0.042+0.958;
end
%Muestra Fallo Bolas
label = label + 1; %Error label
muestras = 0;
estado = 'falloBolas';
erroresaleatorios = [0; 0.958; 0; 0; 1; 0];
disp("**************Generando error Fallo Bolas**************")
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
    disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    erroresaleatorios = (2*maximofallo)*rand(6,1) - maximofallo;
    erroresaleatorios(5) = rand(1)*(errorsuperior - errorinferior) + errorinferior;
    erroresaleatorios(2) = ((maximofallo + erroresaleatorios(2))/2*maximofallo)*0.0084+0.958;
end
%Muestra Fallo Jaula
label = label + 1; %Error label
muestras = 0;
estado = 'falloJaula';
erroresaleatorios = [0; 0.958; 0; 0; 0; 1];
disp("**************Generando error Fallo Jaula**************")
for c=0:numeromuestras
    fprintf('Iteracion %d de %d del proceso %s\n', c, numeromuestras, estado);
    cambiarvalores(vectorerrores, erroresaleatorios);
     disp(erroresaleatorios);
    salida = sim("MotorInduccionFallasRegulador.mdl");
    for a = tiempoinicionda:cortescada:length(Salida_Iab.Data)*1e-5
        datoAB = getsampleusingtime(Salida_Iab,a,a+cortescada);
        datoBC = getsampleusingtime(Salida_Ibc,a,a+cortescada);
        datoCA = getsampleusingtime(Salida_Ica,a,a+cortescada);
        datoAB = resample(datoAB.Data, numeropuntos, length(datoAB.Data));
        datoBC = resample(datoBC.Data, numeropuntos, length(datoBC.Data));
        datoCA = resample(datoCA.Data, numeropuntos, length(datoCA.Data));
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoAB);];  
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoAB transpose(1:size(datoAB,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoBC);];
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoBC transpose(1:size(datoBC,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
        totalmuestras(size(totalmuestras,1)+1,:) = [transpose(datoCA);];     
        totallabels(size(totallabels,1)+1,:) = [label;];
        if int16(100*rand()) == 7
            if muestras < maximomuestras
                matrizcsv = [datoCA transpose(1:size(datoCA,1))];
                csvwrite(strcat(estado, num2str(muestras), ".csv"),matrizcsv);
            end
            muestras = muestras + 1; 
        end
    end
    erroresaleatorios = (2*maximofallo)*rand(6,1) - maximofallo;
    erroresaleatorios(6) = rand(1)*(errorsuperior - errorinferior) + errorinferior;
    erroresaleatorios(2) = ((maximofallo + erroresaleatorios(2))/2*maximofallo)*0.0084+0.958;
end
%Transformada de fourier
totalmuestras = fft(totalmuestras);
totalmuestras = 20*log(abs(totalmuestras)/size(totalmuestras,2));
for i=1:size(totalmuestras,1)
    totalmuestras(i,:) = limiteinferior + ((totalmuestras(i,:) - min(totalmuestras(i,:)))*(limitesuperior - limiteinferior))/(max(totalmuestras(i,:)) - min(totalmuestras(i,:))); 
end
datosEntrenamiento = reshape(totalmuestras, size(totalmuestras,1), 1,1, []);
labelsEntrenamiento = reshape(totallabels, size(totallabels,1), 1,1, []);
h5create('entrenamiento.hdf5','/data',[size(totalmuestras,2), 1,1, size(totalmuestras,1)],'Datatype','double');
h5create('entrenamiento.hdf5','/label',[size(totallabels,2), 1,1, size(totallabels,1)],'Datatype','double');
h5write('entrenamiento.hdf5','/data',permute(datosEntrenamiento,[4 2 3 1]));
h5write('entrenamiento.hdf5','/label',permute(labelsEntrenamiento,[4 2 3 1]));
function cambiarvalores(vectorerrores, vectorvalores)
    %vectorvalores {porcentajeFalloBarrasRotas, PorcentajeFalloEspiras, PorcentajefalloAnilloInterior, PorcentajefalloAnilloExterior, PorcentajefalloBolas, PorcentajeFalloJaulas}
    if vectorvalores(1) > 0 %FalloAnilloInterior
        vectorerrores(1).set('Value', num2str(vectorvalores(1))); %FalloBarrasRotas 
    else
        vectorerrores(1).set('Value', num2str(0)); %FalloBarrasRotas
    end
    vectorerrores(2).set('Value', num2str(vectorvalores(2))); %FalloEspiras
    if vectorvalores(3) > 0 %FalloAnilloInterior
        vectorerrores(3).set('Value', 'on');
        vectorerrores(4).set('Value', num2str(vectorvalores(3)));
    else
        vectorerrores(3).set('Value', 'off');
        vectorerrores(4).set('Value', '0');  
    end
    if vectorvalores(4) > 0 %FalloAnilloExterior
        vectorerrores(5).set('Value', 'on');
        vectorerrores(6).set('Value', num2str(vectorvalores(4)));
    else
        vectorerrores(5).set('Value', 'off');
        vectorerrores(6).set('Value', '0');  
    end
    if vectorvalores(5) > 0
        vectorerrores(7).set('Value', 'on');
        vectorerrores(8).set('Value', num2str(vectorvalores(5)));
    else
        vectorerrores(7).set('Value', 'off');
        vectorerrores(8).set('Value', '0');  
    end
    if vectorvalores(6) > 0
        vectorerrores(9).set('Value', 'on');
        vectorerrores(10).set('Value', num2str(vectorvalores(6)));
    else
        vectorerrores(9).set('Value', 'off');
        vectorerrores(10).set('Value', '0');  
    end
end