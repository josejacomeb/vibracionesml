#include "__cf_MotorInduccionFallasRegulador.h"
#ifndef RTW_HEADER_MotorInduccionFallasRegulador_cap_host_h_
#define RTW_HEADER_MotorInduccionFallasRegulador_cap_host_h_
#ifdef HOST_CAPI_BUILD
#include "rtw_capi.h"
#include "rtw_modelmap.h"
typedef struct { rtwCAPI_ModelMappingInfo mmi ; }
MotorInduccionFallasRegulador_host_DataMapInfo_T ;
#ifdef __cplusplus
extern "C" {
#endif
void MotorInduccionFallasRegulador_host_InitializeDataMapInfo (
MotorInduccionFallasRegulador_host_DataMapInfo_T * dataMap , const char *
path ) ;
#ifdef __cplusplus
}
#endif
#endif
#endif
