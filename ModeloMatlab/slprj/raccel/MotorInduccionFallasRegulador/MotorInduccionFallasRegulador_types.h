#include "__cf_MotorInduccionFallasRegulador.h"
#ifndef RTW_HEADER_MotorInduccionFallasRegulador_types_h_
#define RTW_HEADER_MotorInduccionFallasRegulador_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef DEFINED_TYPEDEF_FOR_struct_Sh3ONz0IKV6ctrRMUkkWc_
#define DEFINED_TYPEDEF_FOR_struct_Sh3ONz0IKV6ctrRMUkkWc_
typedef struct { real_T SwitchResistance [ 12 ] ; real_T SwitchVf [ 24 ] ;
real_T SwitchType [ 12 ] ; real_T SwitchGateInitialValue [ 12 ] ; real_T
EnableUseOfTLC ; real_T OutputsToResetToZero [ 12 ] ; real_T
NoErrorOnMaxIteration ; real_T Ts ; real_T Interpolate ; real_T SaveMatrices
; real_T BufferSize ; boolean_T TBEON ; uint8_T sl_padding0 [ 7 ] ; real_T A
[ 36 ] ; real_T B [ 114 ] ; real_T C [ 90 ] ; real_T D [ 285 ] ; real_T x0 [
6 ] ; } struct_Sh3ONz0IKV6ctrRMUkkWc ;
#endif
typedef struct P_ P ;
#endif
