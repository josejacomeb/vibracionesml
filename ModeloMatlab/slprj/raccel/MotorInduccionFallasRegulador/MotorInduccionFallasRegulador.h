#include "__cf_MotorInduccionFallasRegulador.h"
#ifndef RTW_HEADER_MotorInduccionFallasRegulador_h_
#define RTW_HEADER_MotorInduccionFallasRegulador_h_
#include <stddef.h>
#include <string.h>
#include "rtw_modelmap.h"
#ifndef MotorInduccionFallasRegulador_COMMON_INCLUDES_
#define MotorInduccionFallasRegulador_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "simtarget/slSimTgtSigstreamRTW.h"
#include "simtarget/slSimTgtSlioCoreRTW.h"
#include "simtarget/slSimTgtSlioClientsRTW.h"
#include "simtarget/slSimTgtSlioSdiRTW.h"
#include "sigstream_rtw.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "raccel.h"
#include "slsv_diagnostic_codegen_c_api.h"
#include "rt_logging.h"
#include "dt_info.h"
#include "ext_work.h"
#endif
#include "MotorInduccionFallasRegulador_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#define MODEL_NAME MotorInduccionFallasRegulador
#define NSAMPLE_TIMES (3) 
#define NINPUTS (0)       
#define NOUTPUTS (0)     
#define NBLOCKIO (35) 
#define NUM_ZC_EVENTS (0) 
#ifndef NCSTATES
#define NCSTATES (12)   
#elif NCSTATES != 12
#error Invalid specification of NCSTATES defined in compiler command
#endif
#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm) (*rt_dataMapInfoPtr)
#endif
#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val) (rt_dataMapInfoPtr = &val)
#endif
#ifndef IN_RACCEL_MAIN
#endif
typedef struct { real_T jcrojle2he [ 12 ] ; real_T mqwcf4lld5 ; real_T
j5dc41hgww ; real_T pl34c4fqk0 ; real_T loczlwlvmh ; real_T lrgbcntut3 ;
real_T jbcbq2snz5 ; real_T pdtrdqrwxm ; real_T nl2br3iecm [ 15 ] ; real_T
dthpaqkaud [ 12 ] ; real_T bnrq2sm4fr ; real_T lrgzduc0o1 ; real_T h0xvedellr
; real_T euo4gm213f ; real_T lhf4vfxdhi ; real_T aaplk2ltk5 ; real_T
b5ogduje1l ; real_T iex0zowzo0 ; real_T jilqpvamw4 ; real_T dlhnd0c3ru ;
real_T ow4u2mscgv ; real_T dsktn0mywd ; real_T m233yclh2v ; real_T hggielyofs
; real_T my3leqkuuo ; real_T gcva4vzqtt ; real_T nkrmugj2ta ; real_T
pawly0qgyt ; real_T giowutls13 ; real_T o2nu5uny4z ; real_T j5fbskjjoq ;
real_T dontmjvb5d ; real_T ne1ny2htcf ; real_T agzgcob522 ; real_T ebb2wptoo4
; } B ; typedef struct { real_T pnl0wmnsoc ; real_T awwcp5gzhx ; void *
jbuebthoam [ 19 ] ; struct { void * LoggedData [ 5 ] ; } kuiwrwifyq ; struct
{ void * LoggedData ; } n0nrxyauny ; struct { void * LoggedData ; }
ngj3v5k215 ; struct { void * LoggedData ; } bnovgosvvx ; struct { void *
LoggedData ; } byvvdcayet ; struct { void * LoggedData ; } gg0vcbazuj ;
struct { void * LoggedData [ 5 ] ; } imyoiqhdac ; struct { void * LoggedData
; } bas43napj5 ; int_T bskuuhztya [ 9 ] ; int_T neijnbhbew ; } DW ; typedef
struct { real_T jybb1itsm0 [ 6 ] ; real_T alc1nq4deq ; real_T fb0rqk1lj2 ;
real_T creeazt1yi ; real_T jofusrnood ; real_T pr2avsix55 ; real_T dzehgprsk4
; } X ; typedef struct { real_T jybb1itsm0 [ 6 ] ; real_T alc1nq4deq ; real_T
fb0rqk1lj2 ; real_T creeazt1yi ; real_T jofusrnood ; real_T pr2avsix55 ;
real_T dzehgprsk4 ; } XDot ; typedef struct { boolean_T jybb1itsm0 [ 6 ] ;
boolean_T alc1nq4deq ; boolean_T fb0rqk1lj2 ; boolean_T creeazt1yi ;
boolean_T jofusrnood ; boolean_T pr2avsix55 ; boolean_T dzehgprsk4 ; } XDis ;
typedef struct { rtwCAPI_ModelMappingInfo mmi ; } DataMapInfo ; struct P_ {
real_T Motorinduccin_Bd ; real_T ControlTensinFrecuenciaPWM1_F ; real_T
Motorinduccin_J ; real_T Motorinduccin_Lm ; real_T Motorinduccin_Lr ; real_T
Motorinduccin_Nb ; real_T ControlTensinFrecuenciaPWM1_Nn ; real_T
Motorinduccin_Rs ; real_T ControlTensinFrecuenciaPWM1_V0 ; real_T
ControlTensinFrecuenciaPWM1_Vn ; real_T Inversor_Vs ; real_T Motorinduccin_b
; real_T Motorinduccin_f ; real_T Motorinduccin_p ; real_T
ControlTensinFrecuenciaPWM1_p ; real_T Motorinduccin_pFaex ; real_T
Motorinduccin_pFain ; real_T Motorinduccin_pFball ; real_T
Motorinduccin_pFjau ; real_T Triangle_rep_seq_y [ 4 ] ; real_T
Motorinduccin_tFaex ; real_T Motorinduccin_tFain ; real_T
Motorinduccin_tFball ; real_T Motorinduccin_tFjau ; real_T
Saturation1_LowerSat ; real_T Gain1_Gain ; real_T SwitchCurrents_Value [ 12 ]
; real_T qqq_Value ; real_T qqq_Value_jvekk40ref ; real_T
qqq_Value_p3ijcg553i ; real_T qqq_Value_bk30daw2h4 ; real_T
qqq_Value_eefp2rmfal ; real_T qqq_Value_hadrq2pvon ; real_T
StateSpace_P1_Size [ 2 ] ; real_T StateSpace_P1 [ 36 ] ; real_T
StateSpace_P2_Size [ 2 ] ; real_T StateSpace_P2 [ 114 ] ; real_T
StateSpace_P3_Size [ 2 ] ; real_T StateSpace_P3 [ 90 ] ; real_T
StateSpace_P4_Size [ 2 ] ; real_T StateSpace_P4 [ 285 ] ; real_T
StateSpace_P5_Size [ 2 ] ; real_T StateSpace_P5 [ 6 ] ; real_T
StateSpace_P6_Size [ 2 ] ; real_T StateSpace_P6 ; real_T StateSpace_P7_Size [
2 ] ; real_T StateSpace_P7 [ 12 ] ; real_T StateSpace_P8_Size [ 2 ] ; real_T
StateSpace_P8 [ 12 ] ; real_T StateSpace_P9_Size [ 2 ] ; real_T StateSpace_P9
[ 12 ] ; real_T StateSpace_P10_Size [ 2 ] ; real_T StateSpace_P10 [ 12 ] ;
real_T StateSpace_P11_Size [ 2 ] ; real_T StateSpace_P11 ; real_T
StateSpace_P12_Size [ 2 ] ; real_T StateSpace_P12 ; real_T Integrator_IC ;
real_T Integrator_IC_mt1riet24e ; real_T Gain4_Gain ; real_T
Integrator_IC_hhwrx2xgqc ; real_T Integrator1_IC ; real_T Gain3_Gain ; real_T
Gain_Gain ; real_T Gain5_Gain ; real_T Gain1_Gain_dwlwh44krh ; real_T
Gain6_Gain ; real_T Integrator_IC_gmfqqghfuf ; real_T Gain4_Gain_i0zxrskrml ;
real_T Arranque_Value ; real_T ConsignaVelocidadrpm1_Value ; real_T
Desfase_Value ; real_T Memory_InitialCondition ; real_T Constant_Value ;
real_T TiempoArranque_Value ; real_T Integrator_IC_ohjtmbhm1e ; real_T
Constant_Value_mye2zjdc4g ; real_T LookUpTable1_bp01Data [ 4 ] ; real_T
donotdeletethisgain_Gain ; real_T donotdeletethisgain_Gain_f5rqroac44 ;
real_T donotdeletethisgain_Gain_durm5unh2x ; real_T Gai1_Gain ; real_T
Gain_Gain_btfs54h4bx ; real_T Gain2_Gain ; real_T Rr_Gain ; real_T
Rr_Gain_dbrghggqus ; real_T Constant1_Value ; real_T Gain10_Gain ; real_T
Step_Y0 ; real_T Step_YFinal ; real_T Constant2_Value ; real_T Step1_Y0 ;
real_T Step1_YFinal ; real_T Constant3_Value ; real_T Step2_Y0 ; real_T
Step2_YFinal ; real_T Constant4_Value ; real_T Step3_Y0 ; real_T Step3_YFinal
; real_T Gain_Gain_dkttvxn2he ; real_T Saturation_UpperSat ; real_T
Saturation_LowerSat ; } ; extern const real_T
MotorInduccionFallasRegulador_RGND ; extern const char *
RT_MEMORY_ALLOCATION_ERROR ; extern B rtB ; extern X rtX ; extern DW rtDW ;
extern P rtP ; extern const rtwCAPI_ModelMappingStaticInfo *
MotorInduccionFallasRegulador_GetCAPIStaticMap ( void ) ; extern SimStruct *
const rtS ; extern const int_T gblNumToFiles ; extern const int_T
gblNumFrFiles ; extern const int_T gblNumFrWksBlocks ; extern rtInportTUtable
* gblInportTUtables ; extern const char * gblInportFileName ; extern const
int_T gblNumRootInportBlks ; extern const int_T gblNumModelInputs ; extern
const int_T gblInportDataTypeIdx [ ] ; extern const int_T gblInportDims [ ] ;
extern const int_T gblInportComplex [ ] ; extern const int_T
gblInportInterpoFlag [ ] ; extern const int_T gblInportContinuous [ ] ;
extern const int_T gblParameterTuningTid ; extern DataMapInfo *
rt_dataMapInfoPtr ; extern rtwCAPI_ModelMappingInfo * rt_modelMapInfoPtr ;
void MdlOutputs ( int_T tid ) ; void MdlOutputsParameterSampleTime ( int_T
tid ) ; void MdlUpdate ( int_T tid ) ; void MdlTerminate ( void ) ; void
MdlInitializeSizes ( void ) ; void MdlInitializeSampleTimes ( void ) ;
SimStruct * raccel_register_model ( void ) ;
#endif
