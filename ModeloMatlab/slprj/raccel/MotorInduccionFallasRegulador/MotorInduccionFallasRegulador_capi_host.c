#include "MotorInduccionFallasRegulador_capi_host.h"
static MotorInduccionFallasRegulador_host_DataMapInfo_T root;
static int initialized = 0;
rtwCAPI_ModelMappingInfo *getRootMappingInfo()
{
    if (initialized == 0) {
        initialized = 1;
        MotorInduccionFallasRegulador_host_InitializeDataMapInfo(&(root), "MotorInduccionFallasRegulador");
    }
    return &root.mmi;
}

rtwCAPI_ModelMappingInfo *mexFunction() {return(getRootMappingInfo());}
