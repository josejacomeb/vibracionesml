#include "__cf_MotorInduccionFallasRegulador.h"
#include "rt_logging_mmi.h"
#include "MotorInduccionFallasRegulador_capi.h"
#include <math.h>
#include "MotorInduccionFallasRegulador.h"
#include "MotorInduccionFallasRegulador_private.h"
#include "MotorInduccionFallasRegulador_dt.h"
extern void * CreateDiagnosticAsVoidPtr_wrapper ( const char * id , int nargs
, ... ) ; RTWExtModeInfo * gblRTWExtModeInfo = NULL ; extern boolean_T
gblExtModeStartPktReceived ; void raccelForceExtModeShutdown ( ) { if ( !
gblExtModeStartPktReceived ) { boolean_T stopRequested = false ;
rtExtModeWaitForStartPkt ( gblRTWExtModeInfo , 2 , & stopRequested ) ; }
rtExtModeShutdown ( 2 ) ; }
#include "slsv_diagnostic_codegen_c_api.h"
const int_T gblNumToFiles = 0 ; const int_T gblNumFrFiles = 0 ; const int_T
gblNumFrWksBlocks = 0 ;
#ifdef RSIM_WITH_SOLVER_MULTITASKING
boolean_T gbl_raccel_isMultitasking = 1 ;
#else
boolean_T gbl_raccel_isMultitasking = 0 ;
#endif
boolean_T gbl_raccel_tid01eq = 1 ; int_T gbl_raccel_NumST = 3 ; const char_T
* gbl_raccel_Version = "8.14 (R2018a) 06-Feb-2018" ; void
raccel_setup_MMIStateLog ( SimStruct * S ) {
#ifdef UseMMIDataLogging
rt_FillStateSigInfoFromMMI ( ssGetRTWLogInfo ( S ) , & ssGetErrorStatus ( S )
) ;
#else
UNUSED_PARAMETER ( S ) ;
#endif
} static DataMapInfo rt_dataMapInfo ; DataMapInfo * rt_dataMapInfoPtr = &
rt_dataMapInfo ; rtwCAPI_ModelMappingInfo * rt_modelMapInfoPtr = & (
rt_dataMapInfo . mmi ) ; const char * gblSlvrJacPatternFileName =
 "slprj//raccel//MotorInduccionFallasRegulador//MotorInduccionFallasRegulador_Jpattern.mat"
; const int_T gblNumRootInportBlks = 0 ; const int_T gblNumModelInputs = 0 ;
extern rtInportTUtable * gblInportTUtables ; extern const char *
gblInportFileName ; const int_T gblInportDataTypeIdx [ ] = { - 1 } ; const
int_T gblInportDims [ ] = { - 1 } ; const int_T gblInportComplex [ ] = { - 1
} ; const int_T gblInportInterpoFlag [ ] = { - 1 } ; const int_T
gblInportContinuous [ ] = { - 1 } ;
#include "simstruc.h"
#include "fixedpoint.h"
const real_T MotorInduccionFallasRegulador_RGND = 0.0 ; B rtB ; X rtX ; DW
rtDW ; static SimStruct model_S ; SimStruct * const rtS = & model_S ; real_T
look1_binlxpw ( real_T u0 , const real_T bp0 [ ] , const real_T table [ ] ,
uint32_T maxIndex ) { real_T frac ; uint32_T iRght ; uint32_T iLeft ;
uint32_T bpIdx ; if ( u0 <= bp0 [ 0U ] ) { iLeft = 0U ; frac = ( u0 - bp0 [
0U ] ) / ( bp0 [ 1U ] - bp0 [ 0U ] ) ; } else if ( u0 < bp0 [ maxIndex ] ) {
bpIdx = maxIndex >> 1U ; iLeft = 0U ; iRght = maxIndex ; while ( iRght -
iLeft > 1U ) { if ( u0 < bp0 [ bpIdx ] ) { iRght = bpIdx ; } else { iLeft =
bpIdx ; } bpIdx = ( iRght + iLeft ) >> 1U ; } frac = ( u0 - bp0 [ iLeft ] ) /
( bp0 [ iLeft + 1U ] - bp0 [ iLeft ] ) ; } else { iLeft = maxIndex - 1U ;
frac = ( u0 - bp0 [ maxIndex - 1U ] ) / ( bp0 [ maxIndex ] - bp0 [ maxIndex -
1U ] ) ; } return ( table [ iLeft + 1U ] - table [ iLeft ] ) * frac + table [
iLeft ] ; } void MdlInitialize ( void ) { { SimStruct * rts = ssGetSFunction
( rtS , 0 ) ; sfcnInitializeConditions ( rts ) ; if ( ssGetErrorStatus ( rts
) != ( NULL ) ) return ; } rtX . alc1nq4deq = rtP . Integrator_IC ; rtX .
fb0rqk1lj2 = rtP . Integrator_IC_mt1riet24e ; rtX . creeazt1yi = rtP .
Integrator_IC_hhwrx2xgqc ; rtX . jofusrnood = rtP . Integrator1_IC ; rtX .
pr2avsix55 = rtP . Integrator_IC_gmfqqghfuf ; rtDW . pnl0wmnsoc = rtP .
Memory_InitialCondition ; rtX . dzehgprsk4 = rtP . Integrator_IC_ohjtmbhm1e ;
} void MdlStart ( void ) { { void * * slioCatalogueAddr =
rt_slioCatalogueAddr ( ) ; void * r2 = ( NULL ) ; void * *
pOSigstreamManagerAddr = ( NULL ) ; const int maxErrorBufferSize = 16384 ;
char errMsgCreatingOSigstreamManager [ 16384 ] ; bool
errorCreatingOSigstreamManager = false ; const char *
errorAddingR2SharedResource = ( NULL ) ; * slioCatalogueAddr =
rtwGetNewSlioCatalogue ( rt_GetMatSigLogSelectorFileName ( ) ) ;
errorAddingR2SharedResource = rtwAddR2SharedResource (
rtwGetPointerFromUniquePtr ( rt_slioCatalogue ( ) ) , 1 ) ; if (
errorAddingR2SharedResource != ( NULL ) ) { rtwTerminateSlioCatalogue (
slioCatalogueAddr ) ; * slioCatalogueAddr = ( NULL ) ; ssSetErrorStatus ( rtS
, errorAddingR2SharedResource ) ; return ; } r2 = rtwGetR2SharedResource (
rtwGetPointerFromUniquePtr ( rt_slioCatalogue ( ) ) ) ;
pOSigstreamManagerAddr = rt_GetOSigstreamManagerAddr ( ) ;
errorCreatingOSigstreamManager = rtwOSigstreamManagerCreateInstance (
rt_GetMatSigLogSelectorFileName ( ) , r2 , pOSigstreamManagerAddr ,
errMsgCreatingOSigstreamManager , maxErrorBufferSize ) ; if (
errorCreatingOSigstreamManager ) { * pOSigstreamManagerAddr = ( NULL ) ;
ssSetErrorStatus ( rtS , errMsgCreatingOSigstreamManager ) ; return ; } } {
bool externalInputIsInDatasetFormat = false ; void * pISigstreamManager =
rt_GetISigstreamManager ( ) ; rtwISigstreamManagerGetInputIsInDatasetFormat (
pISigstreamManager , & externalInputIsInDatasetFormat ) ; if (
externalInputIsInDatasetFormat ) { } } memcpy ( & rtB . jcrojle2he [ 0 ] , &
rtP . SwitchCurrents_Value [ 0 ] , 12U * sizeof ( real_T ) ) ; rtB .
mqwcf4lld5 = rtP . Inversor_Vs ; rtB . j5dc41hgww = rtP . qqq_Value ; rtB .
pl34c4fqk0 = rtP . qqq_Value_jvekk40ref ; rtB . loczlwlvmh = rtP .
qqq_Value_p3ijcg553i ; rtB . lrgbcntut3 = rtP . qqq_Value_bk30daw2h4 ; rtB .
jbcbq2snz5 = rtP . qqq_Value_eefp2rmfal ; rtB . pdtrdqrwxm = rtP .
qqq_Value_hadrq2pvon ; { SimStruct * rts = ssGetSFunction ( rtS , 0 ) ;
sfcnStart ( rts ) ; if ( ssGetErrorStatus ( rts ) != ( NULL ) ) return ; } {
static int_T rt_ToWksWidths [ ] = { 1 } ; static int_T rt_ToWksNumDimensions
[ ] = { 1 } ; static int_T rt_ToWksDimensions [ ] = { 1 } ; static boolean_T
rt_ToWksIsVarDims [ ] = { 0 } ; static void * rt_ToWksCurrSigDims [ ] = { (
NULL ) } ; static int_T rt_ToWksCurrSigDimsSize [ ] = { 4 } ; static
BuiltInDTypeId rt_ToWksDataTypeIds [ ] = { SS_DOUBLE } ; static int_T
rt_ToWksComplexSignals [ ] = { 0 } ; static int_T rt_ToWksFrameData [ ] = { 0
} ; static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs [ ] = {
( NULL ) } ; static const char_T * rt_ToWksLabels [ ] = { "Iab" } ; static
RTWLogSignalInfo rt_ToWksSignalInfo = { 1 , rt_ToWksWidths ,
rt_ToWksNumDimensions , rt_ToWksDimensions , rt_ToWksIsVarDims ,
rt_ToWksCurrSigDims , rt_ToWksCurrSigDimsSize , rt_ToWksDataTypeIds ,
rt_ToWksComplexSignals , rt_ToWksFrameData ,
rt_ToWksLoggingPreprocessingFcnPtrs , { rt_ToWksLabels } , ( NULL ) , ( NULL
) , ( NULL ) , { ( NULL ) } , { ( NULL ) } , ( NULL ) , ( NULL ) } ; static
const char_T rt_ToWksBlockName [ ] =
"MotorInduccionFallasRegulador/To Workspace" ; rtDW . n0nrxyauny . LoggedData
= rt_CreateStructLogVar ( ssGetRTWLogInfo ( rtS ) , ssGetTStart ( rtS ) ,
ssGetTFinal ( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS ) ) , "Iab" , 1 , 0 ,
1 , 1.0E-5 , & rt_ToWksSignalInfo , rt_ToWksBlockName ) ; if ( rtDW .
n0nrxyauny . LoggedData == ( NULL ) ) return ; } { static int_T
rt_ToWksWidths [ ] = { 1 } ; static int_T rt_ToWksNumDimensions [ ] = { 1 } ;
static int_T rt_ToWksDimensions [ ] = { 1 } ; static boolean_T
rt_ToWksIsVarDims [ ] = { 0 } ; static void * rt_ToWksCurrSigDims [ ] = { (
NULL ) } ; static int_T rt_ToWksCurrSigDimsSize [ ] = { 4 } ; static
BuiltInDTypeId rt_ToWksDataTypeIds [ ] = { SS_DOUBLE } ; static int_T
rt_ToWksComplexSignals [ ] = { 0 } ; static int_T rt_ToWksFrameData [ ] = { 0
} ; static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs [ ] = {
( NULL ) } ; static const char_T * rt_ToWksLabels [ ] = { "Ica" } ; static
RTWLogSignalInfo rt_ToWksSignalInfo = { 1 , rt_ToWksWidths ,
rt_ToWksNumDimensions , rt_ToWksDimensions , rt_ToWksIsVarDims ,
rt_ToWksCurrSigDims , rt_ToWksCurrSigDimsSize , rt_ToWksDataTypeIds ,
rt_ToWksComplexSignals , rt_ToWksFrameData ,
rt_ToWksLoggingPreprocessingFcnPtrs , { rt_ToWksLabels } , ( NULL ) , ( NULL
) , ( NULL ) , { ( NULL ) } , { ( NULL ) } , ( NULL ) , ( NULL ) } ; static
const char_T rt_ToWksBlockName [ ] =
"MotorInduccionFallasRegulador/To Workspace1" ; rtDW . ngj3v5k215 .
LoggedData = rt_CreateStructLogVar ( ssGetRTWLogInfo ( rtS ) , ssGetTStart (
rtS ) , ssGetTFinal ( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS ) ) , "Ica" ,
1 , 0 , 1 , 1.0E-5 , & rt_ToWksSignalInfo , rt_ToWksBlockName ) ; if ( rtDW .
ngj3v5k215 . LoggedData == ( NULL ) ) return ; } { static int_T
rt_ToWksWidths [ ] = { 1 } ; static int_T rt_ToWksNumDimensions [ ] = { 1 } ;
static int_T rt_ToWksDimensions [ ] = { 1 } ; static boolean_T
rt_ToWksIsVarDims [ ] = { 0 } ; static void * rt_ToWksCurrSigDims [ ] = { (
NULL ) } ; static int_T rt_ToWksCurrSigDimsSize [ ] = { 4 } ; static
BuiltInDTypeId rt_ToWksDataTypeIds [ ] = { SS_DOUBLE } ; static int_T
rt_ToWksComplexSignals [ ] = { 0 } ; static int_T rt_ToWksFrameData [ ] = { 0
} ; static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs [ ] = {
( NULL ) } ; static const char_T * rt_ToWksLabels [ ] = { "Ibc" } ; static
RTWLogSignalInfo rt_ToWksSignalInfo = { 1 , rt_ToWksWidths ,
rt_ToWksNumDimensions , rt_ToWksDimensions , rt_ToWksIsVarDims ,
rt_ToWksCurrSigDims , rt_ToWksCurrSigDimsSize , rt_ToWksDataTypeIds ,
rt_ToWksComplexSignals , rt_ToWksFrameData ,
rt_ToWksLoggingPreprocessingFcnPtrs , { rt_ToWksLabels } , ( NULL ) , ( NULL
) , ( NULL ) , { ( NULL ) } , { ( NULL ) } , ( NULL ) , ( NULL ) } ; static
const char_T rt_ToWksBlockName [ ] =
"MotorInduccionFallasRegulador/To Workspace2" ; rtDW . bnovgosvvx .
LoggedData = rt_CreateStructLogVar ( ssGetRTWLogInfo ( rtS ) , ssGetTStart (
rtS ) , ssGetTFinal ( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS ) ) , "Ibc" ,
1 , 0 , 1 , 1.0E-5 , & rt_ToWksSignalInfo , rt_ToWksBlockName ) ; if ( rtDW .
bnovgosvvx . LoggedData == ( NULL ) ) return ; } { static int_T
rt_ToWksWidths [ ] = { 1 } ; static int_T rt_ToWksNumDimensions [ ] = { 1 } ;
static int_T rt_ToWksDimensions [ ] = { 1 } ; static boolean_T
rt_ToWksIsVarDims [ ] = { 0 } ; static void * rt_ToWksCurrSigDims [ ] = { (
NULL ) } ; static int_T rt_ToWksCurrSigDimsSize [ ] = { 4 } ; static
BuiltInDTypeId rt_ToWksDataTypeIds [ ] = { SS_DOUBLE } ; static int_T
rt_ToWksComplexSignals [ ] = { 0 } ; static int_T rt_ToWksFrameData [ ] = { 0
} ; static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs [ ] = {
( NULL ) } ; static const char_T * rt_ToWksLabels [ ] = { "Velocidad" } ;
static RTWLogSignalInfo rt_ToWksSignalInfo = { 1 , rt_ToWksWidths ,
rt_ToWksNumDimensions , rt_ToWksDimensions , rt_ToWksIsVarDims ,
rt_ToWksCurrSigDims , rt_ToWksCurrSigDimsSize , rt_ToWksDataTypeIds ,
rt_ToWksComplexSignals , rt_ToWksFrameData ,
rt_ToWksLoggingPreprocessingFcnPtrs , { rt_ToWksLabels } , ( NULL ) , ( NULL
) , ( NULL ) , { ( NULL ) } , { ( NULL ) } , ( NULL ) , ( NULL ) } ; static
const char_T rt_ToWksBlockName [ ] =
"MotorInduccionFallasRegulador/To Workspace3" ; rtDW . byvvdcayet .
LoggedData = rt_CreateStructLogVar ( ssGetRTWLogInfo ( rtS ) , ssGetTStart (
rtS ) , ssGetTFinal ( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS ) ) , "N" , 1
, 0 , 1 , 1.0E-5 , & rt_ToWksSignalInfo , rt_ToWksBlockName ) ; if ( rtDW .
byvvdcayet . LoggedData == ( NULL ) ) return ; } { static int_T
rt_ToWksWidths [ ] = { 1 } ; static int_T rt_ToWksNumDimensions [ ] = { 1 } ;
static int_T rt_ToWksDimensions [ ] = { 1 } ; static boolean_T
rt_ToWksIsVarDims [ ] = { 0 } ; static void * rt_ToWksCurrSigDims [ ] = { (
NULL ) } ; static int_T rt_ToWksCurrSigDimsSize [ ] = { 4 } ; static
BuiltInDTypeId rt_ToWksDataTypeIds [ ] = { SS_DOUBLE } ; static int_T
rt_ToWksComplexSignals [ ] = { 0 } ; static int_T rt_ToWksFrameData [ ] = { 0
} ; static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs [ ] = {
( NULL ) } ; static const char_T * rt_ToWksLabels [ ] = { "Par" } ; static
RTWLogSignalInfo rt_ToWksSignalInfo = { 1 , rt_ToWksWidths ,
rt_ToWksNumDimensions , rt_ToWksDimensions , rt_ToWksIsVarDims ,
rt_ToWksCurrSigDims , rt_ToWksCurrSigDimsSize , rt_ToWksDataTypeIds ,
rt_ToWksComplexSignals , rt_ToWksFrameData ,
rt_ToWksLoggingPreprocessingFcnPtrs , { rt_ToWksLabels } , ( NULL ) , ( NULL
) , ( NULL ) , { ( NULL ) } , { ( NULL ) } , ( NULL ) , ( NULL ) } ; static
const char_T rt_ToWksBlockName [ ] =
"MotorInduccionFallasRegulador/To Workspace5" ; rtDW . gg0vcbazuj .
LoggedData = rt_CreateStructLogVar ( ssGetRTWLogInfo ( rtS ) , ssGetTStart (
rtS ) , ssGetTFinal ( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS ) ) , "Mmi" ,
1 , 0 , 1 , 1.0E-5 , & rt_ToWksSignalInfo , rt_ToWksBlockName ) ; if ( rtDW .
gg0vcbazuj . LoggedData == ( NULL ) ) return ; } { int_T dimensions [ 1 ] = {
1 } ; rtDW . bas43napj5 . LoggedData = rt_CreateLogVar ( ssGetRTWLogInfo (
rtS ) , ssGetTStart ( rtS ) , ssGetTFinal ( rtS ) , 0.0 , ( &
ssGetErrorStatus ( rtS ) ) , "t" , SS_DOUBLE , 0 , 0 , 0 , 1 , 1 , dimensions
, NO_LOGVALDIMS , ( NULL ) , ( NULL ) , 0 , 1 , 1.0E-5 , 1 ) ; if ( rtDW .
bas43napj5 . LoggedData == ( NULL ) ) return ; } MdlInitialize ( ) ; } void
MdlOutputs ( int_T tid ) { real_T dme13r0o5j ; real_T fsmkfk3jn2 ; real_T
br5x5mi0bz ; real_T kriiloo4yq ; boolean_T fxzm2hjzrm ; boolean_T mmkduemrh4
; boolean_T ktq4elmhdr ; real_T p4mt2kbjw2 ; real_T ccvpv1nt2r ; real_T
h3figvbkh2_idx_0 ; real_T h3figvbkh2_idx_1 ; real_T h3figvbkh2_idx_2 ; {
SimStruct * rts = ssGetSFunction ( rtS , 0 ) ; sfcnOutputs ( rts , 0 ) ; }
dme13r0o5j = rtP . Motorinduccin_Lm / ( rtP . Motorinduccin_Lm * rtP .
Motorinduccin_Lm - rtP . Motorinduccin_Lr * 0.6277393674 ) * rtX . fb0rqk1lj2
- rtP . Motorinduccin_Lr / ( rtP . Motorinduccin_Lm * rtP . Motorinduccin_Lm
- rtP . Motorinduccin_Lr * 0.6277393674 ) * rtX . alc1nq4deq ; rtB .
bnrq2sm4fr = rtP . Gain4_Gain * dme13r0o5j ; fsmkfk3jn2 = rtP .
Motorinduccin_Lm / ( rtP . Motorinduccin_Lm * rtP . Motorinduccin_Lm - rtP .
Motorinduccin_Lr * 0.6277393674 ) * rtX . jofusrnood - rtP . Motorinduccin_Lr
/ ( rtP . Motorinduccin_Lm * rtP . Motorinduccin_Lm - rtP . Motorinduccin_Lr
* 0.6277393674 ) * rtX . creeazt1yi ; br5x5mi0bz = rtP . Gain3_Gain *
fsmkfk3jn2 ; rtB . lrgzduc0o1 = ( br5x5mi0bz - dme13r0o5j ) * rtP . Gain_Gain
* rtP . Gain5_Gain ; rtB . h0xvedellr = ( br5x5mi0bz + dme13r0o5j ) * rtP .
Gain1_Gain_dwlwh44krh * rtP . Gain6_Gain ; rtB . euo4gm213f = 1.0 / rtP .
Motorinduccin_p * rtX . pr2avsix55 * rtP . Gain4_Gain_i0zxrskrml ; br5x5mi0bz
= rtP . Motorinduccin_Lm / ( rtP . Motorinduccin_Lm * rtP . Motorinduccin_Lm
- rtP . Motorinduccin_Lr * 0.6277393674 ) * rtX . creeazt1yi - 0.6277393674 /
( rtP . Motorinduccin_Lm * rtP . Motorinduccin_Lm - rtP . Motorinduccin_Lr *
0.6277393674 ) * rtX . jofusrnood ; kriiloo4yq = rtP . Motorinduccin_Lm / (
rtP . Motorinduccin_Lm * rtP . Motorinduccin_Lm - rtP . Motorinduccin_Lr *
0.6277393674 ) * rtX . alc1nq4deq - 0.6277393674 / ( rtP . Motorinduccin_Lm *
rtP . Motorinduccin_Lm - rtP . Motorinduccin_Lr * 0.6277393674 ) * rtX .
fb0rqk1lj2 ; rtB . lhf4vfxdhi = 1.5 * rtP . Motorinduccin_p * rtP .
Motorinduccin_Lm * ( fsmkfk3jn2 * kriiloo4yq - dme13r0o5j * br5x5mi0bz ) ; if
( ssIsSampleHit ( rtS , 1 , 0 ) ) { { double locTime = ssGetTaskTime ( rtS ,
1 ) ; ; if ( ssGetLogOutput ( rtS ) ) { { double locTime = ssGetTaskTime (
rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval ( rtliGetLoggingInterval (
ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) , locTime ) ) {
rt_UpdateStructLogVar ( ( StructLogVar * ) rtDW . n0nrxyauny . LoggedData , &
locTime , & rtB . bnrq2sm4fr ) ; } } } } { double locTime = ssGetTaskTime (
rtS , 1 ) ; ; if ( ssGetLogOutput ( rtS ) ) { { double locTime =
ssGetTaskTime ( rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval (
rtliGetLoggingInterval ( ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) ,
locTime ) ) { rt_UpdateStructLogVar ( ( StructLogVar * ) rtDW . ngj3v5k215 .
LoggedData , & locTime , & rtB . h0xvedellr ) ; } } } } { double locTime =
ssGetTaskTime ( rtS , 1 ) ; ; if ( ssGetLogOutput ( rtS ) ) { { double
locTime = ssGetTaskTime ( rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval (
rtliGetLoggingInterval ( ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) ,
locTime ) ) { rt_UpdateStructLogVar ( ( StructLogVar * ) rtDW . bnovgosvvx .
LoggedData , & locTime , & rtB . lrgzduc0o1 ) ; } } } } { double locTime =
ssGetTaskTime ( rtS , 1 ) ; ; if ( ssGetLogOutput ( rtS ) ) { { double
locTime = ssGetTaskTime ( rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval (
rtliGetLoggingInterval ( ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) ,
locTime ) ) { rt_UpdateStructLogVar ( ( StructLogVar * ) rtDW . byvvdcayet .
LoggedData , & locTime , & rtB . euo4gm213f ) ; } } } } { double locTime =
ssGetTaskTime ( rtS , 1 ) ; ; if ( ssGetLogOutput ( rtS ) ) { { double
locTime = ssGetTaskTime ( rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval (
rtliGetLoggingInterval ( ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) ,
locTime ) ) { rt_UpdateStructLogVar ( ( StructLogVar * ) rtDW . gg0vcbazuj .
LoggedData , & locTime , & rtB . lhf4vfxdhi ) ; } } } } rtB . aaplk2ltk5 =
rtDW . pnl0wmnsoc ; if ( rtP . Arranque_Value != 0.0 ) { rtB . b5ogduje1l =
rtB . aaplk2ltk5 ; } else { rtB . b5ogduje1l = rtP . Constant_Value ; } }
p4mt2kbjw2 = ssGetT ( rtS ) ; if ( rtP . Arranque_Value != 0.0 ) { ccvpv1nt2r
= p4mt2kbjw2 - rtB . b5ogduje1l ; } else { ccvpv1nt2r = rtB . b5ogduje1l ; }
ccvpv1nt2r = ( rtP . ConsignaVelocidadrpm1_Value - ( rtP .
ControlTensinFrecuenciaPWM1_F * 60.0 / rtP . ControlTensinFrecuenciaPWM1_p -
rtP . ControlTensinFrecuenciaPWM1_Nn ) ) * ( ccvpv1nt2r / rtP .
TiempoArranque_Value ) + ( rtP . ControlTensinFrecuenciaPWM1_F * 60.0 / rtP .
ControlTensinFrecuenciaPWM1_p - rtP . ControlTensinFrecuenciaPWM1_Nn ) ; if (
ccvpv1nt2r > rtP . ConsignaVelocidadrpm1_Value ) { ccvpv1nt2r = rtP .
ConsignaVelocidadrpm1_Value ; } else { if ( ccvpv1nt2r < rtP .
ControlTensinFrecuenciaPWM1_F * 60.0 / rtP . ControlTensinFrecuenciaPWM1_p -
rtP . ControlTensinFrecuenciaPWM1_Nn ) { ccvpv1nt2r = rtP .
ControlTensinFrecuenciaPWM1_F * 60.0 / rtP . ControlTensinFrecuenciaPWM1_p -
rtP . ControlTensinFrecuenciaPWM1_Nn ; } } rtB . iex0zowzo0 = rtP .
ControlTensinFrecuenciaPWM1_p * 6.2831853071795862 / 60.0 * ccvpv1nt2r ; if (
rtP . Arranque_Value != 0.0 ) { ccvpv1nt2r = rtP .
ControlTensinFrecuenciaPWM1_Vn / ( rtP . ControlTensinFrecuenciaPWM1_Nn *
3.1415926535897931 / 30.0 ) * rtB . iex0zowzo0 + rtP .
ControlTensinFrecuenciaPWM1_V0 ; if ( ccvpv1nt2r > rtP .
ControlTensinFrecuenciaPWM1_Vn ) { ccvpv1nt2r = rtP .
ControlTensinFrecuenciaPWM1_Vn ; } else { if ( ccvpv1nt2r < rtP .
Saturation1_LowerSat ) { ccvpv1nt2r = rtP . Saturation1_LowerSat ; } }
ccvpv1nt2r *= rtP . Gain1_Gain ; h3figvbkh2_idx_0 = 1.0 / rtP .
ControlTensinFrecuenciaPWM1_Vn * ( ccvpv1nt2r * muDoubleScalarSin ( rtX .
dzehgprsk4 ) ) ; h3figvbkh2_idx_1 = muDoubleScalarSin ( rtX . dzehgprsk4 -
rtP . Desfase_Value ) * ccvpv1nt2r * ( 1.0 / rtP .
ControlTensinFrecuenciaPWM1_Vn ) ; h3figvbkh2_idx_2 = muDoubleScalarSin ( rtP
. Desfase_Value + rtX . dzehgprsk4 ) * ccvpv1nt2r * ( 1.0 / rtP .
ControlTensinFrecuenciaPWM1_Vn ) ; } else { h3figvbkh2_idx_0 = rtP .
Arranque_Value ; h3figvbkh2_idx_1 = rtP . Arranque_Value ; h3figvbkh2_idx_2 =
rtP . Arranque_Value ; } ccvpv1nt2r = look1_binlxpw ( muDoubleScalarRem (
ssGetT ( rtS ) - 0.0 , rtP . Constant_Value_mye2zjdc4g ) , rtP .
LookUpTable1_bp01Data , rtP . Triangle_rep_seq_y , 3U ) ; fxzm2hjzrm = (
h3figvbkh2_idx_1 > ccvpv1nt2r ) ; mmkduemrh4 = ( h3figvbkh2_idx_2 >
ccvpv1nt2r ) ; ktq4elmhdr = ( h3figvbkh2_idx_0 > ccvpv1nt2r ) ; if ( rtP .
Arranque_Value != 0.0 ) { rtB . jilqpvamw4 = rtB . aaplk2ltk5 ; } else { rtB
. jilqpvamw4 = p4mt2kbjw2 ; } p4mt2kbjw2 = rtP .
donotdeletethisgain_Gain_f5rqroac44 * rtB . nl2br3iecm [ 13 ] ; ccvpv1nt2r =
rtP . donotdeletethisgain_Gain_durm5unh2x * rtB . nl2br3iecm [ 14 ] ; rtB .
dlhnd0c3ru = ( ktq4elmhdr && ( rtP . Arranque_Value != 0.0 ) ) ; rtB .
ow4u2mscgv = ( ( ! ktq4elmhdr ) && ( rtP . Arranque_Value != 0.0 ) ) ; rtB .
dsktn0mywd = ( fxzm2hjzrm && ( rtP . Arranque_Value != 0.0 ) ) ; rtB .
m233yclh2v = ( ( ! fxzm2hjzrm ) && ( rtP . Arranque_Value != 0.0 ) ) ; rtB .
hggielyofs = ( mmkduemrh4 && ( rtP . Arranque_Value != 0.0 ) ) ; rtB .
my3leqkuuo = ( ( ! mmkduemrh4 ) && ( rtP . Arranque_Value != 0.0 ) ) ; rtB .
gcva4vzqtt = ( 0.0 - rtP . Rr_Gain * kriiloo4yq ) - rtX . jofusrnood * rtX .
pr2avsix55 ; rtB . nkrmugj2ta = rtX . fb0rqk1lj2 * rtX . pr2avsix55 - rtP .
Rr_Gain_dbrghggqus * br5x5mi0bz ; rtB . pawly0qgyt = ( ( rtP .
donotdeletethisgain_Gain * rtB . nl2br3iecm [ 12 ] * rtP . Gai1_Gain -
p4mt2kbjw2 ) - ccvpv1nt2r ) * rtP . Gain_Gain_btfs54h4bx - rtP .
Motorinduccin_Rs * dme13r0o5j ; rtB . giowutls13 = ( p4mt2kbjw2 - ccvpv1nt2r
) * rtP . Gain2_Gain - rtP . Motorinduccin_Rs * fsmkfk3jn2 ; dme13r0o5j = rtP
. Gain10_Gain * rtX . pr2avsix55 ; if ( ( int32_T ) rtP . Constant1_Value ==
1 ) { rtB . o2nu5uny4z = ( 1.0 - rtP . Motorinduccin_Bd / 57.5 *
muDoubleScalarCos ( rtP . Motorinduccin_b ) ) * ( 0.5 * rtP .
Motorinduccin_Nb ) * dme13r0o5j * rtP . Motorinduccin_pFaex ; } else { if (
ssGetTaskTime ( rtS , 0 ) < rtP . Motorinduccin_tFaex ) { ccvpv1nt2r = rtP .
Step_Y0 ; } else { ccvpv1nt2r = rtP . Step_YFinal ; } rtB . o2nu5uny4z = (
1.0 - rtP . Motorinduccin_Bd / 57.5 * muDoubleScalarCos ( rtP .
Motorinduccin_b ) ) * ( 0.5 * rtP . Motorinduccin_Nb ) * dme13r0o5j * rtP .
Motorinduccin_pFaex * ccvpv1nt2r ; } if ( ( int32_T ) rtP . Constant2_Value
== 1 ) { rtB . j5fbskjjoq = ( rtP . Motorinduccin_Bd / 57.5 *
muDoubleScalarCos ( rtP . Motorinduccin_b ) + 1.0 ) * ( 0.5 * rtP .
Motorinduccin_Nb ) * dme13r0o5j * rtP . Motorinduccin_pFain ; } else { if (
ssGetTaskTime ( rtS , 0 ) < rtP . Motorinduccin_tFain ) { ccvpv1nt2r = rtP .
Step1_Y0 ; } else { ccvpv1nt2r = rtP . Step1_YFinal ; } rtB . j5fbskjjoq = (
rtP . Motorinduccin_Bd / 57.5 * muDoubleScalarCos ( rtP . Motorinduccin_b ) +
1.0 ) * ( 0.5 * rtP . Motorinduccin_Nb ) * dme13r0o5j * rtP .
Motorinduccin_pFain * ccvpv1nt2r ; } if ( ( int32_T ) rtP . Constant3_Value
== 1 ) { rtB . dontmjvb5d = 57.5 / rtP . Motorinduccin_Bd *
0.93200244379949226 * dme13r0o5j * rtP . Motorinduccin_pFball ; } else { if (
ssGetTaskTime ( rtS , 0 ) < rtP . Motorinduccin_tFball ) { ccvpv1nt2r = rtP .
Step2_Y0 ; } else { ccvpv1nt2r = rtP . Step2_YFinal ; } rtB . dontmjvb5d =
57.5 / rtP . Motorinduccin_Bd * 0.93200244379949226 * dme13r0o5j * rtP .
Motorinduccin_pFball * ccvpv1nt2r ; } if ( ( int32_T ) rtP . Constant4_Value
== 1 ) { rtB . ne1ny2htcf = ( 1.0 - rtP . Motorinduccin_Bd / 57.5 *
muDoubleScalarCos ( rtP . Motorinduccin_b ) ) * 0.5 * dme13r0o5j * rtP .
Motorinduccin_pFjau ; } else { if ( ssGetTaskTime ( rtS , 0 ) < rtP .
Motorinduccin_tFjau ) { ccvpv1nt2r = rtP . Step3_Y0 ; } else { ccvpv1nt2r =
rtP . Step3_YFinal ; } rtB . ne1ny2htcf = ( 1.0 - rtP . Motorinduccin_Bd /
57.5 * muDoubleScalarCos ( rtP . Motorinduccin_b ) ) * 0.5 * dme13r0o5j * rtP
. Motorinduccin_pFjau * ccvpv1nt2r ; } if ( ssIsSampleHit ( rtS , 1 , 0 ) ) {
} ccvpv1nt2r = rtP . Gain_Gain_dkttvxn2he * ssGetT ( rtS ) ; if ( ccvpv1nt2r
> rtP . Saturation_UpperSat ) { ccvpv1nt2r = rtP . Saturation_UpperSat ; }
else { if ( ccvpv1nt2r < rtP . Saturation_LowerSat ) { ccvpv1nt2r = rtP .
Saturation_LowerSat ; } } rtB . agzgcob522 = ( ( ( ( ( ( rtB . lhf4vfxdhi -
ccvpv1nt2r ) - rtP . Motorinduccin_f / rtP . Motorinduccin_p * rtX .
pr2avsix55 ) - rtB . o2nu5uny4z ) - rtB . j5fbskjjoq ) - rtB . dontmjvb5d ) -
rtB . ne1ny2htcf ) * ( rtP . Motorinduccin_p / rtP . Motorinduccin_J ) ; rtB
. ebb2wptoo4 = ssGetT ( rtS ) ; if ( ssIsSampleHit ( rtS , 1 , 0 ) ) { if (
ssGetLogOutput ( rtS ) ) { { double locTime = ssGetTaskTime ( rtS , 1 ) ; ;
if ( rtwTimeInLoggingInterval ( rtliGetLoggingInterval ( ssGetRootSS ( rtS )
-> mdlInfo -> rtwLogInfo ) , locTime ) ) { rt_UpdateLogVar ( ( LogVar * ) (
LogVar * ) ( rtDW . bas43napj5 . LoggedData ) , & rtB . ebb2wptoo4 , 0 ) ; }
} } } UNUSED_PARAMETER ( tid ) ; } void MdlOutputsTID2 ( int_T tid ) { memcpy
( & rtB . jcrojle2he [ 0 ] , & rtP . SwitchCurrents_Value [ 0 ] , 12U *
sizeof ( real_T ) ) ; rtB . mqwcf4lld5 = rtP . Inversor_Vs ; rtB . j5dc41hgww
= rtP . qqq_Value ; rtB . pl34c4fqk0 = rtP . qqq_Value_jvekk40ref ; rtB .
loczlwlvmh = rtP . qqq_Value_p3ijcg553i ; rtB . lrgbcntut3 = rtP .
qqq_Value_bk30daw2h4 ; rtB . jbcbq2snz5 = rtP . qqq_Value_eefp2rmfal ; rtB .
pdtrdqrwxm = rtP . qqq_Value_hadrq2pvon ; UNUSED_PARAMETER ( tid ) ; } void
MdlUpdate ( int_T tid ) { { SimStruct * rts = ssGetSFunction ( rtS , 0 ) ;
sfcnUpdate ( rts , 0 ) ; if ( ssGetErrorStatus ( rts ) != ( NULL ) ) return ;
} if ( ssIsSampleHit ( rtS , 1 , 0 ) ) { rtDW . pnl0wmnsoc = rtB . jilqpvamw4
; } UNUSED_PARAMETER ( tid ) ; } void MdlUpdateTID2 ( int_T tid ) {
UNUSED_PARAMETER ( tid ) ; } void MdlDerivatives ( void ) { XDot * _rtXdot ;
_rtXdot = ( ( XDot * ) ssGetdX ( rtS ) ) ; { SimStruct * rts = ssGetSFunction
( rtS , 0 ) ; real_T * sfcndX_fx = ( real_T * ) & ( ( XDot * ) ssGetdX ( rtS
) ) -> jybb1itsm0 [ 0 ] ; ssSetdX ( rts , sfcndX_fx ) ; sfcnDerivatives ( rts
) ; if ( ssGetErrorStatus ( rts ) != ( NULL ) ) return ; } _rtXdot ->
alc1nq4deq = rtB . pawly0qgyt ; _rtXdot -> fb0rqk1lj2 = rtB . gcva4vzqtt ;
_rtXdot -> creeazt1yi = rtB . giowutls13 ; _rtXdot -> jofusrnood = rtB .
nkrmugj2ta ; _rtXdot -> pr2avsix55 = rtB . agzgcob522 ; _rtXdot -> dzehgprsk4
= rtB . iex0zowzo0 ; } void MdlProjection ( void ) { } void MdlTerminate (
void ) { { SimStruct * rts = ssGetSFunction ( rtS , 0 ) ; sfcnTerminate ( rts
) ; } if ( rt_slioCatalogue ( ) != ( NULL ) ) { void * * slioCatalogueAddr =
rt_slioCatalogueAddr ( ) ; rtwSaveDatasetsToMatFile (
rtwGetPointerFromUniquePtr ( rt_slioCatalogue ( ) ) ,
rt_GetMatSigstreamLoggingFileName ( ) ) ; rtwTerminateSlioCatalogue (
slioCatalogueAddr ) ; * slioCatalogueAddr = NULL ; } } void
MdlInitializeSizes ( void ) { ssSetNumContStates ( rtS , 12 ) ;
ssSetNumPeriodicContStates ( rtS , 0 ) ; ssSetNumY ( rtS , 0 ) ; ssSetNumU (
rtS , 0 ) ; ssSetDirectFeedThrough ( rtS , 0 ) ; ssSetNumSampleTimes ( rtS ,
2 ) ; ssSetNumBlocks ( rtS , 209 ) ; ssSetNumBlockIO ( rtS , 35 ) ;
ssSetNumBlockParams ( rtS , 701 ) ; } void MdlInitializeSampleTimes ( void )
{ ssSetSampleTime ( rtS , 0 , 0.0 ) ; ssSetSampleTime ( rtS , 1 , 1.0E-5 ) ;
ssSetOffsetTime ( rtS , 0 , 0.0 ) ; ssSetOffsetTime ( rtS , 1 , 0.0 ) ; }
void raccel_set_checksum ( ) { ssSetChecksumVal ( rtS , 0 , 1471793610U ) ;
ssSetChecksumVal ( rtS , 1 , 2818724941U ) ; ssSetChecksumVal ( rtS , 2 ,
3437780729U ) ; ssSetChecksumVal ( rtS , 3 , 1011509190U ) ; }
#if defined(_MSC_VER)
#pragma optimize( "", off )
#endif
SimStruct * raccel_register_model ( void ) { static struct _ssMdlInfo mdlInfo
; ( void ) memset ( ( char * ) rtS , 0 , sizeof ( SimStruct ) ) ; ( void )
memset ( ( char * ) & mdlInfo , 0 , sizeof ( struct _ssMdlInfo ) ) ;
ssSetMdlInfoPtr ( rtS , & mdlInfo ) ; { static time_T mdlPeriod [
NSAMPLE_TIMES ] ; static time_T mdlOffset [ NSAMPLE_TIMES ] ; static time_T
mdlTaskTimes [ NSAMPLE_TIMES ] ; static int_T mdlTsMap [ NSAMPLE_TIMES ] ;
static int_T mdlSampleHits [ NSAMPLE_TIMES ] ; static boolean_T
mdlTNextWasAdjustedPtr [ NSAMPLE_TIMES ] ; static int_T mdlPerTaskSampleHits
[ NSAMPLE_TIMES * NSAMPLE_TIMES ] ; static time_T mdlTimeOfNextSampleHit [
NSAMPLE_TIMES ] ; { int_T i ; for ( i = 0 ; i < NSAMPLE_TIMES ; i ++ ) {
mdlPeriod [ i ] = 0.0 ; mdlOffset [ i ] = 0.0 ; mdlTaskTimes [ i ] = 0.0 ;
mdlTsMap [ i ] = i ; mdlSampleHits [ i ] = 1 ; } } ssSetSampleTimePtr ( rtS ,
& mdlPeriod [ 0 ] ) ; ssSetOffsetTimePtr ( rtS , & mdlOffset [ 0 ] ) ;
ssSetSampleTimeTaskIDPtr ( rtS , & mdlTsMap [ 0 ] ) ; ssSetTPtr ( rtS , &
mdlTaskTimes [ 0 ] ) ; ssSetSampleHitPtr ( rtS , & mdlSampleHits [ 0 ] ) ;
ssSetTNextWasAdjustedPtr ( rtS , & mdlTNextWasAdjustedPtr [ 0 ] ) ;
ssSetPerTaskSampleHitsPtr ( rtS , & mdlPerTaskSampleHits [ 0 ] ) ;
ssSetTimeOfNextSampleHitPtr ( rtS , & mdlTimeOfNextSampleHit [ 0 ] ) ; }
ssSetSolverMode ( rtS , SOLVER_MODE_SINGLETASKING ) ; { ssSetBlockIO ( rtS ,
( ( void * ) & rtB ) ) ; { int32_T i ; for ( i = 0 ; i < 12 ; i ++ ) { rtB .
jcrojle2he [ i ] = 0.0 ; } for ( i = 0 ; i < 15 ; i ++ ) { rtB . nl2br3iecm [
i ] = 0.0 ; } for ( i = 0 ; i < 12 ; i ++ ) { rtB . dthpaqkaud [ i ] = 0.0 ;
} rtB . mqwcf4lld5 = 0.0 ; rtB . j5dc41hgww = 0.0 ; rtB . pl34c4fqk0 = 0.0 ;
rtB . loczlwlvmh = 0.0 ; rtB . lrgbcntut3 = 0.0 ; rtB . jbcbq2snz5 = 0.0 ;
rtB . pdtrdqrwxm = 0.0 ; rtB . bnrq2sm4fr = 0.0 ; rtB . lrgzduc0o1 = 0.0 ;
rtB . h0xvedellr = 0.0 ; rtB . euo4gm213f = 0.0 ; rtB . lhf4vfxdhi = 0.0 ;
rtB . aaplk2ltk5 = 0.0 ; rtB . b5ogduje1l = 0.0 ; rtB . iex0zowzo0 = 0.0 ;
rtB . jilqpvamw4 = 0.0 ; rtB . dlhnd0c3ru = 0.0 ; rtB . ow4u2mscgv = 0.0 ;
rtB . dsktn0mywd = 0.0 ; rtB . m233yclh2v = 0.0 ; rtB . hggielyofs = 0.0 ;
rtB . my3leqkuuo = 0.0 ; rtB . gcva4vzqtt = 0.0 ; rtB . nkrmugj2ta = 0.0 ;
rtB . pawly0qgyt = 0.0 ; rtB . giowutls13 = 0.0 ; rtB . o2nu5uny4z = 0.0 ;
rtB . j5fbskjjoq = 0.0 ; rtB . dontmjvb5d = 0.0 ; rtB . ne1ny2htcf = 0.0 ;
rtB . agzgcob522 = 0.0 ; rtB . ebb2wptoo4 = 0.0 ; } } ssSetDefaultParam ( rtS
, ( real_T * ) & rtP ) ; { real_T * x = ( real_T * ) & rtX ; ssSetContStates
( rtS , x ) ; ( void ) memset ( ( void * ) x , 0 , sizeof ( X ) ) ; } { void
* dwork = ( void * ) & rtDW ; ssSetRootDWork ( rtS , dwork ) ; ( void )
memset ( dwork , 0 , sizeof ( DW ) ) ; rtDW . pnl0wmnsoc = 0.0 ; rtDW .
awwcp5gzhx = 0.0 ; } { static DataTypeTransInfo dtInfo ; ( void ) memset ( (
char_T * ) & dtInfo , 0 , sizeof ( dtInfo ) ) ; ssSetModelMappingInfo ( rtS ,
& dtInfo ) ; dtInfo . numDataTypes = 15 ; dtInfo . dataTypeSizes = &
rtDataTypeSizes [ 0 ] ; dtInfo . dataTypeNames = & rtDataTypeNames [ 0 ] ;
dtInfo . BTransTable = & rtBTransTable ; dtInfo . PTransTable = &
rtPTransTable ; } MotorInduccionFallasRegulador_InitializeDataMapInfo ( ) ;
ssSetIsRapidAcceleratorActive ( rtS , true ) ; ssSetRootSS ( rtS , rtS ) ;
ssSetVersion ( rtS , SIMSTRUCT_VERSION_LEVEL2 ) ; ssSetModelName ( rtS ,
"MotorInduccionFallasRegulador" ) ; ssSetPath ( rtS ,
"MotorInduccionFallasRegulador" ) ; ssSetTStart ( rtS , 0.0 ) ; ssSetTFinal (
rtS , 1.0 ) ; ssSetStepSize ( rtS , 1.0E-5 ) ; ssSetFixedStepSize ( rtS ,
1.0E-5 ) ; { static RTWLogInfo rt_DataLoggingInfo ; rt_DataLoggingInfo .
loggingInterval = NULL ; ssSetRTWLogInfo ( rtS , & rt_DataLoggingInfo ) ; } {
{ static int_T rt_LoggedStateWidths [ ] = { 6 , 1 , 1 , 1 , 1 , 1 , 1 } ;
static int_T rt_LoggedStateNumDimensions [ ] = { 1 , 1 , 1 , 1 , 1 , 1 , 1 }
; static int_T rt_LoggedStateDimensions [ ] = { 6 , 1 , 1 , 1 , 1 , 1 , 1 } ;
static boolean_T rt_LoggedStateIsVarDims [ ] = { 0 , 0 , 0 , 0 , 0 , 0 , 0 }
; static BuiltInDTypeId rt_LoggedStateDataTypeIds [ ] = { SS_DOUBLE ,
SS_DOUBLE , SS_DOUBLE , SS_DOUBLE , SS_DOUBLE , SS_DOUBLE , SS_DOUBLE } ;
static int_T rt_LoggedStateComplexSignals [ ] = { 0 , 0 , 0 , 0 , 0 , 0 , 0 }
; static RTWPreprocessingFcnPtr rt_LoggingStatePreprocessingFcnPtrs [ ] = { (
NULL ) , ( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) } ;
static const char_T * rt_LoggedStateLabels [ ] = { "CSTATE" , "CSTATE" ,
"CSTATE" , "CSTATE" , "CSTATE" , "CSTATE" , "CSTATE" } ; static const char_T
* rt_LoggedStateBlockNames [ ] = {
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ,
"MotorInduccionFallasRegulador/Motor inducción/Ysalfa\n/Integrator" ,
"MotorInduccionFallasRegulador/Motor inducción/Yralfa/Integrator" ,
"MotorInduccionFallasRegulador/Motor inducción/Ysbeta\n/Integrator" ,
"MotorInduccionFallasRegulador/Motor inducción/Yrbeta\n/Integrator1" ,
"MotorInduccionFallasRegulador/Motor inducción/wr/Integrator" ,
 "MotorInduccionFallasRegulador/Control \nTensión//Frecuencia\nPWM1/Generador de Tensiones/Integrator"
} ; static const char_T * rt_LoggedStateNames [ ] = { "" , "" , "" , "" , ""
, "" , "" } ; static boolean_T rt_LoggedStateCrossMdlRef [ ] = { 0 , 0 , 0 ,
0 , 0 , 0 , 0 } ; static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert [ ] =
{ { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE
, SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 ,
0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 ,
0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 ,
SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE ,
SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } } ; static RTWLogSignalInfo
rt_LoggedStateSignalInfo = { 7 , rt_LoggedStateWidths ,
rt_LoggedStateNumDimensions , rt_LoggedStateDimensions ,
rt_LoggedStateIsVarDims , ( NULL ) , ( NULL ) , rt_LoggedStateDataTypeIds ,
rt_LoggedStateComplexSignals , ( NULL ) , rt_LoggingStatePreprocessingFcnPtrs
, { rt_LoggedStateLabels } , ( NULL ) , ( NULL ) , ( NULL ) , {
rt_LoggedStateBlockNames } , { rt_LoggedStateNames } ,
rt_LoggedStateCrossMdlRef , rt_RTWLogDataTypeConvert } ; static void *
rt_LoggedStateSignalPtrs [ 7 ] ; rtliSetLogXSignalPtrs ( ssGetRTWLogInfo (
rtS ) , ( LogSignalPtrsType ) rt_LoggedStateSignalPtrs ) ;
rtliSetLogXSignalInfo ( ssGetRTWLogInfo ( rtS ) , & rt_LoggedStateSignalInfo
) ; rt_LoggedStateSignalPtrs [ 0 ] = ( void * ) & rtX . jybb1itsm0 [ 0 ] ;
rt_LoggedStateSignalPtrs [ 1 ] = ( void * ) & rtX . alc1nq4deq ;
rt_LoggedStateSignalPtrs [ 2 ] = ( void * ) & rtX . fb0rqk1lj2 ;
rt_LoggedStateSignalPtrs [ 3 ] = ( void * ) & rtX . creeazt1yi ;
rt_LoggedStateSignalPtrs [ 4 ] = ( void * ) & rtX . jofusrnood ;
rt_LoggedStateSignalPtrs [ 5 ] = ( void * ) & rtX . pr2avsix55 ;
rt_LoggedStateSignalPtrs [ 6 ] = ( void * ) & rtX . dzehgprsk4 ; }
rtliSetLogT ( ssGetRTWLogInfo ( rtS ) , "tmp_raccel_tout" ) ; rtliSetLogX (
ssGetRTWLogInfo ( rtS ) , "tmp_raccel_xout" ) ; rtliSetLogXFinal (
ssGetRTWLogInfo ( rtS ) , "xFinal" ) ; rtliSetLogVarNameModifier (
ssGetRTWLogInfo ( rtS ) , "none" ) ; rtliSetLogFormat ( ssGetRTWLogInfo ( rtS
) , 2 ) ; rtliSetLogMaxRows ( ssGetRTWLogInfo ( rtS ) , 0 ) ;
rtliSetLogDecimation ( ssGetRTWLogInfo ( rtS ) , 1 ) ; rtliSetLogY (
ssGetRTWLogInfo ( rtS ) , "" ) ; rtliSetLogYSignalInfo ( ssGetRTWLogInfo (
rtS ) , ( NULL ) ) ; rtliSetLogYSignalPtrs ( ssGetRTWLogInfo ( rtS ) , ( NULL
) ) ; } { static struct _ssStatesInfo2 statesInfo2 ; ssSetStatesInfo2 ( rtS ,
& statesInfo2 ) ; } { static ssPeriodicStatesInfo periodicStatesInfo ;
ssSetPeriodicStatesInfo ( rtS , & periodicStatesInfo ) ; } { static
ssSolverInfo slvrInfo ; static boolean_T contStatesDisabled [ 12 ] ;
ssSetSolverInfo ( rtS , & slvrInfo ) ; ssSetSolverName ( rtS , "ode5" ) ;
ssSetVariableStepSolver ( rtS , 0 ) ; ssSetSolverConsistencyChecking ( rtS ,
0 ) ; ssSetSolverAdaptiveZcDetection ( rtS , 0 ) ;
ssSetSolverRobustResetMethod ( rtS , 0 ) ; ssSetSolverStateProjection ( rtS ,
0 ) ; ssSetSolverMassMatrixType ( rtS , ( ssMatrixType ) 0 ) ;
ssSetSolverMassMatrixNzMax ( rtS , 0 ) ; ssSetModelOutputs ( rtS , MdlOutputs
) ; ssSetModelLogData ( rtS , rt_UpdateTXYLogVars ) ;
ssSetModelLogDataIfInInterval ( rtS , rt_UpdateTXXFYLogVars ) ;
ssSetModelUpdate ( rtS , MdlUpdate ) ; ssSetModelDerivatives ( rtS ,
MdlDerivatives ) ; ssSetTNextTid ( rtS , INT_MIN ) ; ssSetTNext ( rtS ,
rtMinusInf ) ; ssSetSolverNeedsReset ( rtS ) ; ssSetNumNonsampledZCs ( rtS ,
0 ) ; ssSetContStateDisabled ( rtS , contStatesDisabled ) ; }
ssSetChecksumVal ( rtS , 0 , 1471793610U ) ; ssSetChecksumVal ( rtS , 1 ,
2818724941U ) ; ssSetChecksumVal ( rtS , 2 , 3437780729U ) ; ssSetChecksumVal
( rtS , 3 , 1011509190U ) ; { static const sysRanDType rtAlwaysEnabled =
SUBSYS_RAN_BC_ENABLE ; static RTWExtModeInfo rt_ExtModeInfo ; static const
sysRanDType * systemRan [ 8 ] ; gblRTWExtModeInfo = & rt_ExtModeInfo ;
ssSetRTWExtModeInfo ( rtS , & rt_ExtModeInfo ) ;
rteiSetSubSystemActiveVectorAddresses ( & rt_ExtModeInfo , systemRan ) ;
systemRan [ 0 ] = & rtAlwaysEnabled ; systemRan [ 1 ] = & rtAlwaysEnabled ;
systemRan [ 2 ] = & rtAlwaysEnabled ; systemRan [ 3 ] = & rtAlwaysEnabled ;
systemRan [ 4 ] = & rtAlwaysEnabled ; systemRan [ 5 ] = & rtAlwaysEnabled ;
systemRan [ 6 ] = & rtAlwaysEnabled ; systemRan [ 7 ] = & rtAlwaysEnabled ;
rteiSetModelMappingInfoPtr ( ssGetRTWExtModeInfo ( rtS ) , &
ssGetModelMappingInfo ( rtS ) ) ; rteiSetChecksumsPtr ( ssGetRTWExtModeInfo (
rtS ) , ssGetChecksums ( rtS ) ) ; rteiSetTPtr ( ssGetRTWExtModeInfo ( rtS )
, ssGetTPtr ( rtS ) ) ; } ssSetNumSFunctions ( rtS , 1 ) ; { static SimStruct
childSFunctions [ 1 ] ; static SimStruct * childSFunctionPtrs [ 1 ] ; ( void
) memset ( ( void * ) & childSFunctions [ 0 ] , 0 , sizeof ( childSFunctions
) ) ; ssSetSFunctions ( rtS , & childSFunctionPtrs [ 0 ] ) ; ssSetSFunction (
rtS , 0 , & childSFunctions [ 0 ] ) ; { SimStruct * rts = ssGetSFunction (
rtS , 0 ) ; static time_T sfcnPeriod [ 1 ] ; static time_T sfcnOffset [ 1 ] ;
static int_T sfcnTsMap [ 1 ] ; ( void ) memset ( ( void * ) sfcnPeriod , 0 ,
sizeof ( time_T ) * 1 ) ; ( void ) memset ( ( void * ) sfcnOffset , 0 ,
sizeof ( time_T ) * 1 ) ; ssSetSampleTimePtr ( rts , & sfcnPeriod [ 0 ] ) ;
ssSetOffsetTimePtr ( rts , & sfcnOffset [ 0 ] ) ; ssSetSampleTimeTaskIDPtr (
rts , sfcnTsMap ) ; { static struct _ssBlkInfo2 _blkInfo2 ; struct
_ssBlkInfo2 * blkInfo2 = & _blkInfo2 ; ssSetBlkInfo2Ptr ( rts , blkInfo2 ) ;
} { static struct _ssPortInfo2 _portInfo2 ; struct _ssPortInfo2 * portInfo2 =
& _portInfo2 ; _ssSetBlkInfo2PortInfo2Ptr ( rts , portInfo2 ) ; }
ssSetMdlInfoPtr ( rts , ssGetMdlInfoPtr ( rtS ) ) ; { static struct
_ssSFcnModelMethods2 methods2 ; ssSetModelMethods2 ( rts , & methods2 ) ; } {
static struct _ssSFcnModelMethods3 methods3 ; ssSetModelMethods3 ( rts , &
methods3 ) ; } { static struct _ssSFcnModelMethods4 methods4 ;
ssSetModelMethods4 ( rts , & methods4 ) ; } { static struct _ssStatesInfo2
statesInfo2 ; static ssPeriodicStatesInfo periodicStatesInfo ;
ssSetStatesInfo2 ( rts , & statesInfo2 ) ; ssSetPeriodicStatesInfo ( rts , &
periodicStatesInfo ) ; } { static struct _ssPortInputs inputPortInfo [ 2 ] ;
_ssSetNumInputPorts ( rts , 2 ) ; ssSetPortInfoForInputs ( rts , &
inputPortInfo [ 0 ] ) ; { static struct _ssInPortUnit inputPortUnits [ 2 ] ;
_ssSetPortInfo2ForInputUnits ( rts , & inputPortUnits [ 0 ] ) ; }
ssSetInputPortUnit ( rts , 0 , 0 ) ; ssSetInputPortUnit ( rts , 1 , 0 ) ; {
static struct _ssInPortCoSimAttribute inputPortCoSimAttribute [ 2 ] ;
_ssSetPortInfo2ForInputCoSimAttribute ( rts , & inputPortCoSimAttribute [ 0 ]
) ; } ssSetInputPortIsContinuousQuantity ( rts , 0 , 0 ) ;
ssSetInputPortIsContinuousQuantity ( rts , 1 , 0 ) ; { static real_T const *
sfcnUPtrs [ 19 ] ; { int_T i1 ; const real_T * u0 = & rtB . jcrojle2he [ 0 ]
; for ( i1 = 0 ; i1 < 12 ; i1 ++ ) { sfcnUPtrs [ i1 ] = & u0 [ i1 ] ; }
sfcnUPtrs [ 12 ] = & rtB . mqwcf4lld5 ; sfcnUPtrs [ 13 ] = & rtB . j5dc41hgww
; sfcnUPtrs [ 14 ] = & rtB . pl34c4fqk0 ; sfcnUPtrs [ 15 ] = & rtB .
loczlwlvmh ; sfcnUPtrs [ 16 ] = & rtB . lrgbcntut3 ; sfcnUPtrs [ 17 ] = & rtB
. jbcbq2snz5 ; sfcnUPtrs [ 18 ] = & rtB . pdtrdqrwxm ; }
ssSetInputPortSignalPtrs ( rts , 0 , ( InputPtrsType ) & sfcnUPtrs [ 0 ] ) ;
_ssSetInputPortNumDimensions ( rts , 0 , 1 ) ; ssSetInputPortWidth ( rts , 0
, 19 ) ; } { static real_T const * sfcnUPtrs [ 12 ] ; sfcnUPtrs [ 0 ] = & rtB
. dlhnd0c3ru ; sfcnUPtrs [ 1 ] = & rtB . dsktn0mywd ; sfcnUPtrs [ 2 ] = & rtB
. hggielyofs ; sfcnUPtrs [ 3 ] = & rtB . ow4u2mscgv ; sfcnUPtrs [ 4 ] = & rtB
. m233yclh2v ; sfcnUPtrs [ 5 ] = & rtB . my3leqkuuo ; sfcnUPtrs [ 6 ] = (
real_T * ) & MotorInduccionFallasRegulador_RGND ; sfcnUPtrs [ 7 ] = ( real_T
* ) & MotorInduccionFallasRegulador_RGND ; sfcnUPtrs [ 8 ] = ( real_T * ) &
MotorInduccionFallasRegulador_RGND ; sfcnUPtrs [ 9 ] = ( real_T * ) &
MotorInduccionFallasRegulador_RGND ; sfcnUPtrs [ 10 ] = ( real_T * ) &
MotorInduccionFallasRegulador_RGND ; sfcnUPtrs [ 11 ] = ( real_T * ) &
MotorInduccionFallasRegulador_RGND ; ssSetInputPortSignalPtrs ( rts , 1 , (
InputPtrsType ) & sfcnUPtrs [ 0 ] ) ; _ssSetInputPortNumDimensions ( rts , 1
, 1 ) ; ssSetInputPortWidth ( rts , 1 , 12 ) ; } } { static struct
_ssPortOutputs outputPortInfo [ 2 ] ; ssSetPortInfoForOutputs ( rts , &
outputPortInfo [ 0 ] ) ; _ssSetNumOutputPorts ( rts , 2 ) ; { static struct
_ssOutPortUnit outputPortUnits [ 2 ] ; _ssSetPortInfo2ForOutputUnits ( rts ,
& outputPortUnits [ 0 ] ) ; } ssSetOutputPortUnit ( rts , 0 , 0 ) ;
ssSetOutputPortUnit ( rts , 1 , 0 ) ; { static struct
_ssOutPortCoSimAttribute outputPortCoSimAttribute [ 2 ] ;
_ssSetPortInfo2ForOutputCoSimAttribute ( rts , & outputPortCoSimAttribute [ 0
] ) ; } ssSetOutputPortIsContinuousQuantity ( rts , 0 , 0 ) ;
ssSetOutputPortIsContinuousQuantity ( rts , 1 , 0 ) ; {
_ssSetOutputPortNumDimensions ( rts , 0 , 1 ) ; ssSetOutputPortWidth ( rts ,
0 , 15 ) ; ssSetOutputPortSignal ( rts , 0 , ( ( real_T * ) rtB . nl2br3iecm
) ) ; } { _ssSetOutputPortNumDimensions ( rts , 1 , 1 ) ;
ssSetOutputPortWidth ( rts , 1 , 12 ) ; ssSetOutputPortSignal ( rts , 1 , ( (
real_T * ) rtB . dthpaqkaud ) ) ; } } ssSetContStates ( rts , & rtX .
jybb1itsm0 [ 0 ] ) ; ssSetModelName ( rts , "State-Space" ) ; ssSetPath ( rts
, "MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ;
if ( ssGetRTModel ( rtS ) == ( NULL ) ) { ssSetParentSS ( rts , rtS ) ;
ssSetRootSS ( rts , ssGetRootSS ( rtS ) ) ; } else { ssSetRTModel ( rts ,
ssGetRTModel ( rtS ) ) ; ssSetParentSS ( rts , ( NULL ) ) ; ssSetRootSS ( rts
, rts ) ; } ssSetVersion ( rts , SIMSTRUCT_VERSION_LEVEL2 ) ; { static
mxArray * sfcnParams [ 12 ] ; ssSetSFcnParamsCount ( rts , 12 ) ;
ssSetSFcnParamsPtr ( rts , & sfcnParams [ 0 ] ) ; ssSetSFcnParam ( rts , 0 ,
( mxArray * ) rtP . StateSpace_P1_Size ) ; ssSetSFcnParam ( rts , 1 , (
mxArray * ) rtP . StateSpace_P2_Size ) ; ssSetSFcnParam ( rts , 2 , ( mxArray
* ) rtP . StateSpace_P3_Size ) ; ssSetSFcnParam ( rts , 3 , ( mxArray * ) rtP
. StateSpace_P4_Size ) ; ssSetSFcnParam ( rts , 4 , ( mxArray * ) rtP .
StateSpace_P5_Size ) ; ssSetSFcnParam ( rts , 5 , ( mxArray * ) rtP .
StateSpace_P6_Size ) ; ssSetSFcnParam ( rts , 6 , ( mxArray * ) rtP .
StateSpace_P7_Size ) ; ssSetSFcnParam ( rts , 7 , ( mxArray * ) rtP .
StateSpace_P8_Size ) ; ssSetSFcnParam ( rts , 8 , ( mxArray * ) rtP .
StateSpace_P9_Size ) ; ssSetSFcnParam ( rts , 9 , ( mxArray * ) rtP .
StateSpace_P10_Size ) ; ssSetSFcnParam ( rts , 10 , ( mxArray * ) rtP .
StateSpace_P11_Size ) ; ssSetSFcnParam ( rts , 11 , ( mxArray * ) rtP .
StateSpace_P12_Size ) ; } ssSetRWork ( rts , ( real_T * ) & rtDW . awwcp5gzhx
) ; ssSetIWork ( rts , ( int_T * ) & rtDW . bskuuhztya [ 0 ] ) ; ssSetPWork (
rts , ( void * * ) & rtDW . jbuebthoam [ 0 ] ) ; { static struct
_ssDWorkRecord dWorkRecord [ 4 ] ; static struct _ssDWorkAuxRecord
dWorkAuxRecord [ 4 ] ; ssSetSFcnDWork ( rts , dWorkRecord ) ;
ssSetSFcnDWorkAux ( rts , dWorkAuxRecord ) ; _ssSetNumDWork ( rts , 4 ) ;
ssSetDWorkWidth ( rts , 0 , 1 ) ; ssSetDWorkDataType ( rts , 0 , SS_INTEGER )
; ssSetDWorkComplexSignal ( rts , 0 , 0 ) ; ssSetDWork ( rts , 0 , & rtDW .
neijnbhbew ) ; ssSetDWorkWidth ( rts , 1 , 1 ) ; ssSetDWorkDataType ( rts , 1
, SS_DOUBLE ) ; ssSetDWorkComplexSignal ( rts , 1 , 0 ) ; ssSetDWork ( rts ,
1 , & rtDW . awwcp5gzhx ) ; ssSetDWorkWidth ( rts , 2 , 9 ) ;
ssSetDWorkDataType ( rts , 2 , SS_INTEGER ) ; ssSetDWorkComplexSignal ( rts ,
2 , 0 ) ; ssSetDWork ( rts , 2 , & rtDW . bskuuhztya [ 0 ] ) ;
ssSetDWorkWidth ( rts , 3 , 19 ) ; ssSetDWorkDataType ( rts , 3 , SS_POINTER
) ; ssSetDWorkComplexSignal ( rts , 3 , 0 ) ; ssSetDWork ( rts , 3 , & rtDW .
jbuebthoam [ 0 ] ) ; } ssSetModeVector ( rts , ( int_T * ) & rtDW .
neijnbhbew ) ; sfun_spssw_contc ( rts ) ; sfcnInitializeSizes ( rts ) ;
sfcnInitializeSampleTimes ( rts ) ; ssSetSampleTime ( rts , 0 , 0.0 ) ;
ssSetOffsetTime ( rts , 0 , 0.0 ) ; sfcnTsMap [ 0 ] = 0 ;
ssSetNumNonsampledZCs ( rts , 0 ) ; _ssSetInputPortConnected ( rts , 0 , 1 )
; _ssSetInputPortConnected ( rts , 1 , 1 ) ; _ssSetOutputPortConnected ( rts
, 0 , 1 ) ; _ssSetOutputPortConnected ( rts , 1 , 1 ) ;
_ssSetOutputPortBeingMerged ( rts , 0 , 0 ) ; _ssSetOutputPortBeingMerged (
rts , 1 , 0 ) ; ssSetInputPortBufferDstPort ( rts , 0 , - 1 ) ;
ssSetInputPortBufferDstPort ( rts , 1 , - 1 ) ; } } return rtS ; }
#if defined(_MSC_VER)
#pragma optimize( "", on )
#endif
const int_T gblParameterTuningTid = 2 ; void MdlOutputsParameterSampleTime (
int_T tid ) { MdlOutputsTID2 ( tid ) ; }
