#include "__cf_MotorInduccionFallasRegulador.h"
#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "MotorInduccionFallasRegulador_capi_host.h"
#define sizeof(s) ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el) ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s) (s)    
#else
#include "builtin_typeid_types.h"
#include "MotorInduccionFallasRegulador.h"
#include "MotorInduccionFallasRegulador_capi.h"
#include "MotorInduccionFallasRegulador_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST                  
#define TARGET_STRING(s)               (NULL)                    
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif
static const rtwCAPI_Signals rtBlockSignals [ ] = { { 0 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Clock" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 ,
0 , 0 } , { 1 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/Generador de Tensiones/rpm to rad//s"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 2 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/tiempo memory/Memory"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 1 } , { 3 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/tiempo memory/Sitch"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 4 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/tiempo memory/Switch1"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 1 } , { 5 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Mmi/Gain" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 0 } , { 6 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/Gain4"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 7 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/Gain5"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 8 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/Gain6"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 9 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Yralfa/Add" ) , TARGET_STRING
( "" ) , 0 , 0 , 0 , 0 , 0 } , { 10 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Yrbeta /Add2" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 11 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Ysalfa /Add1" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 12 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Ysbeta /Add" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 13 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Gain1" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 0 } , { 14 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Rodamiento" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 15 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Rodamiento1" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 16 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Rodamiento2" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 17 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Rodamiento3" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 18 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 1 , 0 , 0 } , { 19 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "" ) , 1 , 0 , 2 , 0 , 0 } , { 20 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe/Model/qqq" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 2 } , { 21 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe1/Model/qqq" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 2 } , { 22 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe2/Model/qqq" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 2 } , { 23 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe3/Model/qqq" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 2 } , { 24 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe4/Model/qqq" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 2 } , { 25 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe5/Model/qqq" ) , TARGET_STRING (
"" ) , 0 , 0 , 0 , 0 , 2 } , { 26 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Ideal Switch/Model/Data Type Conversion"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 27 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Ideal Switch1/Model/Data Type Conversion"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 28 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Ideal Switch2/Model/Data Type Conversion"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 29 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Ideal Switch3/Model/Data Type Conversion"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 30 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Ideal Switch4/Model/Data Type Conversion"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 31 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Ideal Switch5/Model/Data Type Conversion"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 32 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Vs/Model/DC" ) , TARGET_STRING ( "" )
, 0 , 0 , 0 , 0 , 2 } , { 33 , 0 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/rad2rpm/Gain4"
) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 34 , 0 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/Sources/SwitchCurrents"
) , TARGET_STRING ( "" ) , 0 , 0 , 2 , 0 , 2 } , { 0 , 0 , ( NULL ) , ( NULL
) , 0 , 0 , 0 , 0 , 0 } } ; static const rtwCAPI_BlockParameters
rtBlockParameters [ ] = { { 35 , TARGET_STRING (
"MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1" ) ,
TARGET_STRING ( "Vn" ) , 0 , 0 , 0 } , { 36 , TARGET_STRING (
"MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1" ) ,
TARGET_STRING ( "V0" ) , 0 , 0 , 0 } , { 37 , TARGET_STRING (
"MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1" ) ,
TARGET_STRING ( "Nn" ) , 0 , 0 , 0 } , { 38 , TARGET_STRING (
"MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1" ) ,
TARGET_STRING ( "p" ) , 0 , 0 , 0 } , { 39 , TARGET_STRING (
"MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1" ) ,
TARGET_STRING ( "F" ) , 0 , 0 , 0 } , { 40 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor" ) , TARGET_STRING ( "Vs" ) , 0 , 0 ,
0 } , { 41 , TARGET_STRING ( "MotorInduccionFallasRegulador/Motor inducción"
) , TARGET_STRING ( "Rs" ) , 0 , 0 , 0 } , { 42 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "Lr" ) ,
0 , 0 , 0 } , { 43 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "Lm" ) ,
0 , 0 , 0 } , { 44 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "p" ) ,
0 , 0 , 0 } , { 45 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "f" ) ,
0 , 0 , 0 } , { 46 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "J" ) ,
0 , 0 , 0 } , { 47 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "Nb" ) ,
0 , 0 , 0 } , { 48 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "Bd" ) ,
0 , 0 , 0 } , { 49 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "b" ) ,
0 , 0 , 0 } , { 50 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "tFaex"
) , 0 , 0 , 0 } , { 51 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "tFain"
) , 0 , 0 , 0 } , { 52 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "tFball"
) , 0 , 0 , 0 } , { 53 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "tFjau"
) , 0 , 0 , 0 } , { 54 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "pFaex"
) , 0 , 0 , 0 } , { 55 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "pFain"
) , 0 , 0 , 0 } , { 56 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "pFball"
) , 0 , 0 , 0 } , { 57 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción" ) , TARGET_STRING ( "pFjau"
) , 0 , 0 , 0 } , { 58 , TARGET_STRING (
"MotorInduccionFallasRegulador/Arranque" ) , TARGET_STRING ( "Value" ) , 0 ,
0 , 0 } , { 59 , TARGET_STRING (
"MotorInduccionFallasRegulador/Consigna Velocidad (rpm)1" ) , TARGET_STRING (
"Value" ) , 0 , 0 , 0 } , { 60 , TARGET_STRING (
"MotorInduccionFallasRegulador/Tiempo  Arranque" ) , TARGET_STRING ( "Value"
) , 0 , 0 , 0 } , { 61 , TARGET_STRING (
"MotorInduccionFallasRegulador/Subsystem/Gain" ) , TARGET_STRING ( "Gain" ) ,
0 , 0 , 0 } , { 62 , TARGET_STRING (
"MotorInduccionFallasRegulador/Subsystem/Saturation" ) , TARGET_STRING (
"UpperLimit" ) , 0 , 0 , 0 } , { 63 , TARGET_STRING (
"MotorInduccionFallasRegulador/Subsystem/Saturation" ) , TARGET_STRING (
"LowerLimit" ) , 0 , 0 , 0 } , { 64 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/Generador de Tensiones/Desfase"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 65 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/Generador de Tensiones/Gain1"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 66 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/Generador de Tensiones/Integrator"
) , TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 67 , TARGET_STRING
(
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/Generador de Tensiones/Saturation1"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 68 , TARGET_STRING (
"MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/PWM/Triangle"
) , TARGET_STRING ( "rep_seq_y" ) , 0 , 3 , 0 } , { 69 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/tiempo memory/Constant"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 70 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/tiempo memory/Memory"
) , TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 71 , TARGET_STRING
(
 "MotorInduccionFallasRegulador/Inversor/Voltage Measurement/do not delete this gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 72 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Voltage Measurement1/do not delete this gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 73 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Inversor/Voltage Measurement2/do not delete this gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 74 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/2//3 /Gain" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 75 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/2//3 /Gain1" ) ,
TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 76 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/2//3 /Gain3" ) ,
TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 77 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/3//2/Gai1" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 78 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/3//2/Gain" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 79 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/3//2/Gain2" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 80 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/Gain4"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 81 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/Gain5"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 82 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/Gain6"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 83 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Yralfa/Rr" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 84 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Yralfa/Integrator" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 85 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Yrbeta /Rr" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 86 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Yrbeta /Integrator1" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 87 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Ysalfa /Integrator" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 88 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/Ysbeta /Integrator" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 89 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Constant1" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 90 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Constant2" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 91 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Constant3" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 92 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Constant4" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 93 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Gain10" ) , TARGET_STRING
( "Gain" ) , 0 , 0 , 0 } , { 94 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Integrator" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 95 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step" ) , TARGET_STRING (
"Before" ) , 0 , 0 , 0 } , { 96 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step" ) , TARGET_STRING (
"After" ) , 0 , 0 , 0 } , { 97 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step1" ) , TARGET_STRING (
"Before" ) , 0 , 0 , 0 } , { 98 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step1" ) , TARGET_STRING (
"After" ) , 0 , 0 , 0 } , { 99 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step2" ) , TARGET_STRING (
"Before" ) , 0 , 0 , 0 } , { 100 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step2" ) , TARGET_STRING (
"After" ) , 0 , 0 , 0 } , { 101 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step3" ) , TARGET_STRING (
"Before" ) , 0 , 0 , 0 } , { 102 , TARGET_STRING (
"MotorInduccionFallasRegulador/Motor inducción/wr/Step3" ) , TARGET_STRING (
"After" ) , 0 , 0 , 0 } , { 103 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P1" ) , 0 , 4 , 0 } , { 104 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P2" ) , 0 , 5 , 0 } , { 105 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P3" ) , 0 , 6 , 0 } , { 106 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P4" ) , 0 , 7 , 0 } , { 107 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P5" ) , 0 , 8 , 0 } , { 108 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P6" ) , 0 , 0 , 0 } , { 109 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P7" ) , 0 , 9 , 0 } , { 110 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P8" ) , 0 , 9 , 0 } , { 111 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P9" ) , 0 , 2 , 0 } , { 112 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P10" ) , 0 , 9 , 0 } , { 113 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P11" ) , 0 , 0 , 0 } , { 114 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/State-Space" ) ,
TARGET_STRING ( "P12" ) , 0 , 0 , 0 } , { 115 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/PWM/Triangle/Constant"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 116 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Control  Tensión//Frecuencia PWM1/PWM/Triangle/Look-Up Table1"
) , TARGET_STRING ( "BreakpointsForDimension1" ) , 0 , 3 , 0 } , { 117 ,
TARGET_STRING ( "MotorInduccionFallasRegulador/Inversor/Dioe/Model/qqq" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 118 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe1/Model/qqq" ) , TARGET_STRING (
"Value" ) , 0 , 0 , 0 } , { 119 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe2/Model/qqq" ) , TARGET_STRING (
"Value" ) , 0 , 0 , 0 } , { 120 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe3/Model/qqq" ) , TARGET_STRING (
"Value" ) , 0 , 0 , 0 } , { 121 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe4/Model/qqq" ) , TARGET_STRING (
"Value" ) , 0 , 0 , 0 } , { 122 , TARGET_STRING (
"MotorInduccionFallasRegulador/Inversor/Dioe5/Model/qqq" ) , TARGET_STRING (
"Value" ) , 0 , 0 , 0 } , { 123 , TARGET_STRING (
 "MotorInduccionFallasRegulador/Motor inducción/Transformación  corrientes fase - linea y velocidad rad//s - rpm/rad2rpm/Gain4"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 124 , TARGET_STRING (
"MotorInduccionFallasRegulador/powergui/EquivalentModel1/Sources/SwitchCurrents"
) , TARGET_STRING ( "Value" ) , 0 , 2 , 0 } , { 0 , ( NULL ) , ( NULL ) , 0 ,
0 , 0 } } ; static const rtwCAPI_ModelParameters rtModelParameters [ ] = { {
0 , ( NULL ) , 0 , 0 , 0 } } ;
#ifndef HOST_CAPI_BUILD
static void * rtDataAddrMap [ ] = { & rtB . ebb2wptoo4 , & rtB . iex0zowzo0 ,
& rtB . aaplk2ltk5 , & rtB . jilqpvamw4 , & rtB . b5ogduje1l , & rtB .
lhf4vfxdhi , & rtB . bnrq2sm4fr , & rtB . lrgzduc0o1 , & rtB . h0xvedellr , &
rtB . gcva4vzqtt , & rtB . nkrmugj2ta , & rtB . pawly0qgyt , & rtB .
giowutls13 , & rtB . agzgcob522 , & rtB . o2nu5uny4z , & rtB . j5fbskjjoq , &
rtB . dontmjvb5d , & rtB . ne1ny2htcf , & rtB . nl2br3iecm [ 0 ] , & rtB .
dthpaqkaud [ 0 ] , & rtB . j5dc41hgww , & rtB . pl34c4fqk0 , & rtB .
loczlwlvmh , & rtB . lrgbcntut3 , & rtB . jbcbq2snz5 , & rtB . pdtrdqrwxm , &
rtB . dlhnd0c3ru , & rtB . dsktn0mywd , & rtB . hggielyofs , & rtB .
ow4u2mscgv , & rtB . m233yclh2v , & rtB . my3leqkuuo , & rtB . mqwcf4lld5 , &
rtB . euo4gm213f , & rtB . jcrojle2he [ 0 ] , & rtP .
ControlTensinFrecuenciaPWM1_Vn , & rtP . ControlTensinFrecuenciaPWM1_V0 , &
rtP . ControlTensinFrecuenciaPWM1_Nn , & rtP . ControlTensinFrecuenciaPWM1_p
, & rtP . ControlTensinFrecuenciaPWM1_F , & rtP . Inversor_Vs , & rtP .
Motorinduccin_Rs , & rtP . Motorinduccin_Lr , & rtP . Motorinduccin_Lm , &
rtP . Motorinduccin_p , & rtP . Motorinduccin_f , & rtP . Motorinduccin_J , &
rtP . Motorinduccin_Nb , & rtP . Motorinduccin_Bd , & rtP . Motorinduccin_b ,
& rtP . Motorinduccin_tFaex , & rtP . Motorinduccin_tFain , & rtP .
Motorinduccin_tFball , & rtP . Motorinduccin_tFjau , & rtP .
Motorinduccin_pFaex , & rtP . Motorinduccin_pFain , & rtP .
Motorinduccin_pFball , & rtP . Motorinduccin_pFjau , & rtP . Arranque_Value ,
& rtP . ConsignaVelocidadrpm1_Value , & rtP . TiempoArranque_Value , & rtP .
Gain_Gain_dkttvxn2he , & rtP . Saturation_UpperSat , & rtP .
Saturation_LowerSat , & rtP . Desfase_Value , & rtP . Gain1_Gain , & rtP .
Integrator_IC_ohjtmbhm1e , & rtP . Saturation1_LowerSat , & rtP .
Triangle_rep_seq_y [ 0 ] , & rtP . Constant_Value , & rtP .
Memory_InitialCondition , & rtP . donotdeletethisgain_Gain , & rtP .
donotdeletethisgain_Gain_f5rqroac44 , & rtP .
donotdeletethisgain_Gain_durm5unh2x , & rtP . Gain_Gain , & rtP .
Gain1_Gain_dwlwh44krh , & rtP . Gain3_Gain , & rtP . Gai1_Gain , & rtP .
Gain_Gain_btfs54h4bx , & rtP . Gain2_Gain , & rtP . Gain4_Gain , & rtP .
Gain5_Gain , & rtP . Gain6_Gain , & rtP . Rr_Gain , & rtP .
Integrator_IC_mt1riet24e , & rtP . Rr_Gain_dbrghggqus , & rtP .
Integrator1_IC , & rtP . Integrator_IC , & rtP . Integrator_IC_hhwrx2xgqc , &
rtP . Constant1_Value , & rtP . Constant2_Value , & rtP . Constant3_Value , &
rtP . Constant4_Value , & rtP . Gain10_Gain , & rtP .
Integrator_IC_gmfqqghfuf , & rtP . Step_Y0 , & rtP . Step_YFinal , & rtP .
Step1_Y0 , & rtP . Step1_YFinal , & rtP . Step2_Y0 , & rtP . Step2_YFinal , &
rtP . Step3_Y0 , & rtP . Step3_YFinal , & rtP . StateSpace_P1 [ 0 ] , & rtP .
StateSpace_P2 [ 0 ] , & rtP . StateSpace_P3 [ 0 ] , & rtP . StateSpace_P4 [ 0
] , & rtP . StateSpace_P5 [ 0 ] , & rtP . StateSpace_P6 , & rtP .
StateSpace_P7 [ 0 ] , & rtP . StateSpace_P8 [ 0 ] , & rtP . StateSpace_P9 [ 0
] , & rtP . StateSpace_P10 [ 0 ] , & rtP . StateSpace_P11 , & rtP .
StateSpace_P12 , & rtP . Constant_Value_mye2zjdc4g , & rtP .
LookUpTable1_bp01Data [ 0 ] , & rtP . qqq_Value , & rtP .
qqq_Value_jvekk40ref , & rtP . qqq_Value_p3ijcg553i , & rtP .
qqq_Value_bk30daw2h4 , & rtP . qqq_Value_eefp2rmfal , & rtP .
qqq_Value_hadrq2pvon , & rtP . Gain4_Gain_i0zxrskrml , & rtP .
SwitchCurrents_Value [ 0 ] , } ; static int32_T * rtVarDimsAddrMap [ ] = { (
NULL ) } ;
#endif
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap [ ] = { { "double" ,
"real_T" , 0 , 0 , sizeof ( real_T ) , SS_DOUBLE , 0 , 0 } } ;
#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif
static TARGET_CONST rtwCAPI_ElementMap rtElementMap [ ] = { { ( NULL ) , 0 ,
0 , 0 , 0 } , } ; static const rtwCAPI_DimensionMap rtDimensionMap [ ] = { {
rtwCAPI_SCALAR , 0 , 2 , 0 } , { rtwCAPI_VECTOR , 2 , 2 , 0 } , {
rtwCAPI_VECTOR , 4 , 2 , 0 } , { rtwCAPI_VECTOR , 6 , 2 , 0 } , {
rtwCAPI_MATRIX_COL_MAJOR , 8 , 2 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 10 , 2
, 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 12 , 2 , 0 } , {
rtwCAPI_MATRIX_COL_MAJOR , 14 , 2 , 0 } , { rtwCAPI_VECTOR , 16 , 2 , 0 } , {
rtwCAPI_VECTOR , 18 , 2 , 0 } } ; static const uint_T rtDimensionArray [ ] =
{ 1 , 1 , 15 , 1 , 12 , 1 , 1 , 4 , 6 , 6 , 6 , 19 , 15 , 6 , 15 , 19 , 6 , 1
, 1 , 12 } ; static const real_T rtcapiStoredFloats [ ] = { 0.0 , 1.0E-5 } ;
static const rtwCAPI_FixPtMap rtFixPtMap [ ] = { { ( NULL ) , ( NULL ) ,
rtwCAPI_FIX_RESERVED , 0 , 0 , 0 } , } ; static const rtwCAPI_SampleTimeMap
rtSampleTimeMap [ ] = { { ( const void * ) & rtcapiStoredFloats [ 0 ] , (
const void * ) & rtcapiStoredFloats [ 0 ] , 0 , 0 } , { ( const void * ) &
rtcapiStoredFloats [ 1 ] , ( const void * ) & rtcapiStoredFloats [ 0 ] , 1 ,
0 } , { ( NULL ) , ( NULL ) , 2 , 0 } } ; static
rtwCAPI_ModelMappingStaticInfo mmiStatic = { { rtBlockSignals , 35 , ( NULL )
, 0 , ( NULL ) , 0 } , { rtBlockParameters , 90 , rtModelParameters , 0 } , {
( NULL ) , 0 } , { rtDataTypeMap , rtDimensionMap , rtFixPtMap , rtElementMap
, rtSampleTimeMap , rtDimensionArray } , "float" , { 1471793610U ,
2818724941U , 3437780729U , 1011509190U } , ( NULL ) , 0 , 0 } ; const
rtwCAPI_ModelMappingStaticInfo *
MotorInduccionFallasRegulador_GetCAPIStaticMap ( void ) { return & mmiStatic
; }
#ifndef HOST_CAPI_BUILD
void MotorInduccionFallasRegulador_InitializeDataMapInfo ( void ) {
rtwCAPI_SetVersion ( ( * rt_dataMapInfoPtr ) . mmi , 1 ) ;
rtwCAPI_SetStaticMap ( ( * rt_dataMapInfoPtr ) . mmi , & mmiStatic ) ;
rtwCAPI_SetLoggingStaticMap ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetDataAddressMap ( ( * rt_dataMapInfoPtr ) . mmi , rtDataAddrMap ) ;
rtwCAPI_SetVarDimsAddressMap ( ( * rt_dataMapInfoPtr ) . mmi ,
rtVarDimsAddrMap ) ; rtwCAPI_SetInstanceLoggingInfo ( ( * rt_dataMapInfoPtr )
. mmi , ( NULL ) ) ; rtwCAPI_SetChildMMIArray ( ( * rt_dataMapInfoPtr ) . mmi
, ( NULL ) ) ; rtwCAPI_SetChildMMIArrayLen ( ( * rt_dataMapInfoPtr ) . mmi ,
0 ) ; }
#else
#ifdef __cplusplus
extern "C" {
#endif
void MotorInduccionFallasRegulador_host_InitializeDataMapInfo (
MotorInduccionFallasRegulador_host_DataMapInfo_T * dataMap , const char *
path ) { rtwCAPI_SetVersion ( dataMap -> mmi , 1 ) ; rtwCAPI_SetStaticMap (
dataMap -> mmi , & mmiStatic ) ; rtwCAPI_SetDataAddressMap ( dataMap -> mmi ,
NULL ) ; rtwCAPI_SetVarDimsAddressMap ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetPath ( dataMap -> mmi , path ) ; rtwCAPI_SetFullPath ( dataMap ->
mmi , NULL ) ; rtwCAPI_SetChildMMIArray ( dataMap -> mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( dataMap -> mmi , 0 ) ; }
#ifdef __cplusplus
}
#endif
#endif
