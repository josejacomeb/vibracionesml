/********************************************************************************
** Form generated from reading UI file 'ventanaprincipal.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAPRINCIPAL_H
#define UI_VENTANAPRINCIPAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_VentanaPrincipal
{
public:
    QWidget *centralWidget;
    QCustomPlot *graficoTiempo;
    QCustomPlot *graficoFrecuencia;
    QLabel *label;
    QLabel *label_2;
    QPushButton *botonAbrir;
    QLabel *rutaArchivo;
    QLabel *label_3;
    QCustomPlot *graficoEntradaRed;
    QLabel *salidaClasificacion;
    QLabel *label_4;
    QLabel *label_de;
    QLabel *label_ku;
    QLabel *label_me;
    QLabel *label_rms;
    QLabel *label_sk;
    QLabel *label_va;
    QLabel *label_11;
    QLabel *salidaNeurofuzzy;
    QLabel *me;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *de;
    QLabel *ku;
    QLabel *rms;
    QLabel *sk;
    QLabel *va;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *VentanaPrincipal)
    {
        if (VentanaPrincipal->objectName().isEmpty())
            VentanaPrincipal->setObjectName(QStringLiteral("VentanaPrincipal"));
        VentanaPrincipal->resize(1024, 720);
        centralWidget = new QWidget(VentanaPrincipal);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        graficoTiempo = new QCustomPlot(centralWidget);
        graficoTiempo->setObjectName(QStringLiteral("graficoTiempo"));
        graficoTiempo->setGeometry(QRect(20, 60, 501, 211));
        graficoFrecuencia = new QCustomPlot(centralWidget);
        graficoFrecuencia->setObjectName(QStringLiteral("graficoFrecuencia"));
        graficoFrecuencia->setGeometry(QRect(20, 290, 481, 211));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 40, 131, 18));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 270, 181, 18));
        botonAbrir = new QPushButton(centralWidget);
        botonAbrir->setObjectName(QStringLiteral("botonAbrir"));
        botonAbrir->setGeometry(QRect(910, 0, 111, 34));
        rutaArchivo = new QLabel(centralWidget);
        rutaArchivo->setObjectName(QStringLiteral("rutaArchivo"));
        rutaArchivo->setGeometry(QRect(30, 10, 871, 18));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 500, 181, 18));
        graficoEntradaRed = new QCustomPlot(centralWidget);
        graficoEntradaRed->setObjectName(QStringLiteral("graficoEntradaRed"));
        graficoEntradaRed->setGeometry(QRect(20, 520, 481, 121));
        salidaClasificacion = new QLabel(centralWidget);
        salidaClasificacion->setObjectName(QStringLiteral("salidaClasificacion"));
        salidaClasificacion->setGeometry(QRect(540, 130, 441, 20));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(520, 500, 181, 18));
        label_de = new QLabel(centralWidget);
        label_de->setObjectName(QStringLiteral("label_de"));
        label_de->setGeometry(QRect(520, 530, 141, 20));
        label_ku = new QLabel(centralWidget);
        label_ku->setObjectName(QStringLiteral("label_ku"));
        label_ku->setGeometry(QRect(520, 550, 141, 20));
        label_me = new QLabel(centralWidget);
        label_me->setObjectName(QStringLiteral("label_me"));
        label_me->setGeometry(QRect(520, 570, 141, 20));
        label_rms = new QLabel(centralWidget);
        label_rms->setObjectName(QStringLiteral("label_rms"));
        label_rms->setGeometry(QRect(520, 590, 141, 20));
        label_sk = new QLabel(centralWidget);
        label_sk->setObjectName(QStringLiteral("label_sk"));
        label_sk->setGeometry(QRect(520, 610, 141, 20));
        label_va = new QLabel(centralWidget);
        label_va->setObjectName(QStringLiteral("label_va"));
        label_va->setGeometry(QRect(520, 630, 141, 20));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(740, 500, 221, 20));
        salidaNeurofuzzy = new QLabel(centralWidget);
        salidaNeurofuzzy->setObjectName(QStringLiteral("salidaNeurofuzzy"));
        salidaNeurofuzzy->setGeometry(QRect(750, 530, 141, 20));
        me = new QLabel(centralWidget);
        me->setObjectName(QStringLiteral("me"));
        me->setGeometry(QRect(670, 570, 141, 20));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(660, 530, 91, 122));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        de = new QLabel(widget);
        de->setObjectName(QStringLiteral("de"));

        verticalLayout->addWidget(de);

        ku = new QLabel(widget);
        ku->setObjectName(QStringLiteral("ku"));

        verticalLayout->addWidget(ku);

        rms = new QLabel(widget);
        rms->setObjectName(QStringLiteral("rms"));

        verticalLayout->addWidget(rms);

        sk = new QLabel(widget);
        sk->setObjectName(QStringLiteral("sk"));

        verticalLayout->addWidget(sk);

        va = new QLabel(widget);
        va->setObjectName(QStringLiteral("va"));

        verticalLayout->addWidget(va);

        VentanaPrincipal->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(VentanaPrincipal);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 32));
        VentanaPrincipal->setMenuBar(menuBar);
        mainToolBar = new QToolBar(VentanaPrincipal);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        VentanaPrincipal->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(VentanaPrincipal);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        VentanaPrincipal->setStatusBar(statusBar);
        toolBar = new QToolBar(VentanaPrincipal);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        VentanaPrincipal->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(VentanaPrincipal);

        QMetaObject::connectSlotsByName(VentanaPrincipal);
    } // setupUi

    void retranslateUi(QMainWindow *VentanaPrincipal)
    {
        VentanaPrincipal->setWindowTitle(QApplication::translate("VentanaPrincipal", "MCSA Deep Learning", Q_NULLPTR));
        label->setText(QApplication::translate("VentanaPrincipal", "Dominio del tiempo", Q_NULLPTR));
        label_2->setText(QApplication::translate("VentanaPrincipal", "Dominio de la frecuencia", Q_NULLPTR));
        botonAbrir->setText(QApplication::translate("VentanaPrincipal", "Abrir Archivo", Q_NULLPTR));
        rutaArchivo->setText(QString());
        label_3->setText(QApplication::translate("VentanaPrincipal", "Entrada Red", Q_NULLPTR));
        salidaClasificacion->setText(QString());
        label_4->setText(QApplication::translate("VentanaPrincipal", "<html><head/><body><p><span style=\" font-weight:600;\">Parametros Neurofuzzy</span></p></body></html>", Q_NULLPTR));
        label_de->setText(QApplication::translate("VentanaPrincipal", "Desviaci\303\263n est\303\241ndar", Q_NULLPTR));
        label_ku->setText(QApplication::translate("VentanaPrincipal", "Kurtois", Q_NULLPTR));
        label_me->setText(QApplication::translate("VentanaPrincipal", "Media", Q_NULLPTR));
        label_rms->setText(QApplication::translate("VentanaPrincipal", "RMS", Q_NULLPTR));
        label_sk->setText(QApplication::translate("VentanaPrincipal", "Skewness", Q_NULLPTR));
        label_va->setText(QApplication::translate("VentanaPrincipal", "Varianza", Q_NULLPTR));
        label_11->setText(QApplication::translate("VentanaPrincipal", "<html><head/><body><p><span style=\" font-weight:600;\">Clase Preddici\303\263n Neuro Fuzzy</span></p></body></html>", Q_NULLPTR));
        salidaNeurofuzzy->setText(QString());
        me->setText(QString());
        de->setText(QString());
        ku->setText(QString());
        rms->setText(QString());
        sk->setText(QString());
        va->setText(QString());
        toolBar->setWindowTitle(QApplication::translate("VentanaPrincipal", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class VentanaPrincipal: public Ui_VentanaPrincipal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAPRINCIPAL_H
