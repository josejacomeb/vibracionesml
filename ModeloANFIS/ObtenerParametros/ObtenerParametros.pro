TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lboost_system -lboost_filesystem -lhdf5_cpp

INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu


unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += hdf5 hdf5-serial


unix:!macx: LIBS += -L$$PWD/../../FuzzyLite/release/bin/ -lfuzzylite

INCLUDEPATH += $$PWD/../../FuzzyLite
DEPENDPATH += $$PWD/../../FuzzyLite
