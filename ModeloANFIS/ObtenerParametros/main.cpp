#include <iostream>
#include <string>
#include <boost/math/distributions.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/kurtosis.hpp>
#include <boost/accumulators/statistics/skewness.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <fstream>
#include <boost/filesystem.hpp>
#include <H5Cpp.h>
#include <H5File.h>
#include <cstdlib>

using namespace boost::filesystem;
using namespace H5;
using namespace std;
using namespace boost::accumulators;


const int 	DIM0 = 4;	               // dataset dimensions
const int 	DIM1 = 6;
string nombrearchivo = "";
string nombresalida = "datos.dat";
int numeromuestras = 0;
struct ParametrosEstadisticos
{
    float media = 0, desviacionestandar = 0, varianza = 0, kurtois = 0, skewness = 0, rms = 0;
};


void ayuda(){
    cout << "" << "Escribir generador de ondas y la función" <<  endl;
    cout << "--f Especificar el nombre del archivo que contiene los datasets" << endl;
    cout << "--o Especificar el nombre de archivo de salida" << endl;
    cout << "Ejemplo: ./ObtenerParametros --f datosentrenamiento.h5list --o dataset.dat" << endl;
}
void obtenerDatasets(string nombredataset,  vector<vector<ParametrosEstadisticos>> caracteristicas){
    std::ofstream ofs (nombredataset, std::ofstream::out);
    for(size_t i = 0; i < caracteristicas.size(); i++){
        //Iteracion clase
        for(size_t j = 0; j < caracteristicas.at(i).size(); j++){
            //Iteracion elementos
            ofs << caracteristicas.at(i).at(j).desviacionestandar << " " << caracteristicas.at(i).at(j).kurtois <<
                   " " << caracteristicas.at(i).at(j).media << " " << caracteristicas.at(i).at(j).rms << " " <<
                   caracteristicas.at(i).at(j).skewness << " " << caracteristicas.at(i).at(j).varianza << " " << i << endl;
        }
    }

    ofs.close();
    cout << "==========================================================" << endl;
    cout << "Finalizo de escribir el archivo: " << nombredataset << endl;
    cout << "==========================================================" << endl;
}

int main(int argc, char* argv[]){
    cout << "**********************************************" << endl;
    cout << "Obtener los parametros estadisticos para Fuzzy" << endl;
    cout << "**********************************************" << endl;


    cout << argc << endl;
    if(argc < 3){
        ayuda();
        return 1;
    }

    for(int i = 0; i < argc; i++){
        if(std::string(argv[i]) == "--f"){
            if(i + 1 < argc){
                nombrearchivo = string(argv[i+1]);
                cout << "Cambiado el valor de nombre de archivo a: " << nombrearchivo << endl;
            }
            else{
                cout << "Indique el valor despues del nombre de archivo" << endl;
                return -1;
            }
        }
        else if(std::string(argv[i]) == "--o"){
            if(i + 1 < argc){
                nombresalida = string(argv[i+1]);
                cout << "Cambiado el valor de nombre de salida a: " << nombresalida << endl;
            }
            else{
                cout << "Indique el valor despues del nombre de salida del archivo" << endl;
                return -1;
            }
        }
    }
    if(nombrearchivo == "") {
        cout << "Especifique un nombre de archivo con --f" << endl;
        return -1;
    }
    std::ifstream archivoentrada(nombrearchivo);
    path rutaArchivo(nombrearchivo);
    const path rutaRelativa = rutaArchivo.parent_path();
    std::string linea;
    const H5std_string	NombreDatasetLabel("label");
    const H5std_string	NombreDatasetData("data");
    const int RANK = 4; //4 dimensiones del blob
    const int clases = 3;    //0 sin fallos, 1 barras rotas, 2 asimetria eje

    vector<vector<ParametrosEstadisticos>> caracteristicas(clases, vector<ParametrosEstadisticos>()); //clases, matematicas
    vector<vector<ParametrosEstadisticos>> caracteristicasTestSet(clases, vector<ParametrosEstadisticos>()); //clases, matematicas
    ParametrosEstadisticos parametrosindividual;
    while (std::getline(archivoentrada, linea))
    {
        if(linea.length()==0) break;
        cout <<  "Leyendo ruta: " << canonical(rutaRelativa).string() + "/" + linea << endl;
        const H5std_string	nombreArchivoDataset(canonical(rutaRelativa).string() + "/" + linea);
        try{
            H5File archivo(nombreArchivoDataset, H5F_ACC_RDWR);
            DataSet datasetData = archivo.openDataSet(NombreDatasetData);
            DataSet datasetLabel = archivo.openDataSet(NombreDatasetLabel);
            DataSpace filespaceData = datasetData.getSpace();
            DataSpace filespaceLabel = datasetLabel.getSpace();
            int rankData = filespaceData.getSimpleExtentNdims();
            int rankLabels = filespaceLabel.getSimpleExtentNdims();
            hsize_t dimslabels[RANK], dimsdata[RANK];    // dataset dimensions
            rankLabels = filespaceLabel.getSimpleExtentDims( dimslabels );
            rankData = filespaceData.getSimpleExtentDims( dimsdata );
            DataSpace mSpaceData(RANK, dimsdata);
            DataSpace mSpaceLabels(RANK, dimslabels);
            double datosLabel[dimslabels[0]][dimslabels[1]][dimslabels[2]][dimslabels[3]];
            double datosData[dimsdata[0]][dimsdata[1]][dimsdata[2]][dimsdata[3]];

            datasetData.read( datosData, PredType::NATIVE_DOUBLE, mSpaceData, filespaceData );
            datasetLabel.read( datosLabel, PredType::NATIVE_DOUBLE, mSpaceLabels, filespaceLabel );
            accumulator_set<float,stats<tag::min, tag::max, tag::sum, tag::count, tag::mean, tag::kurtosis, tag::variance, tag::skewness>> acumulador; //Sirve para obtener las características de de la
            for(size_t i = 0; i < dimsdata[0]-2; i++){
                acumulador = {};
                for(size_t j = 0; j < dimsdata[3]; j++){
                    //cout << "Linea: " << linea << "i: " << i  << " - " << dimsdata[0] << " j: " << j << "-" << dimsdata[3]  << " Datos:[   " << i << "][" << dimsdata[1] << "][" << dimsdata[2] << "][" << j  << "]: " << datosData[i][dimsdata[1]][dimsdata[2]][j] << endl;
                    acumulador(datosData[i][dimsdata[1]][dimsdata[2]][j]);
                }
                parametrosindividual.media = mean(acumulador);
                parametrosindividual.kurtois = kurtosis(acumulador);
                parametrosindividual.desviacionestandar = sqrt(variance(acumulador));
                parametrosindividual.varianza = variance(acumulador);
                parametrosindividual.skewness = skewness(acumulador);
                parametrosindividual.rms = sqrt(abs(sum(acumulador))/boost::accumulators::count(acumulador));
                caracteristicas.at(int(datosLabel[i][dimslabels[1]][dimslabels[2]][dimslabels[3]])).push_back(parametrosindividual);
            }
        }    catch(FileIException error)
        {
            error.printErrorStack();
            return -1;
        }

    }
    struct maximosValores
    {
        struct parametros{
            float maximo = -10000, minimo = 10000, sumatoria = 0;
        };
        parametros media, desviacionestandar, varianza, kurtois, skewness, rms;

    };
    vector<maximosValores> caracteristicasFinales(clases);
    for(size_t i = 0; i < caracteristicas.size(); i++){
        caracteristicasFinales.at(i).media.maximo = -10000;
        caracteristicasFinales.at(i).media.minimo = 100000;
        caracteristicasFinales.at(i).media.sumatoria = 0;
        caracteristicasFinales.at(i).desviacionestandar.maximo = -10000;
        caracteristicasFinales.at(i).desviacionestandar.minimo = 10000;
        caracteristicasFinales.at(i).desviacionestandar.sumatoria = 0;
        caracteristicasFinales.at(i).varianza.maximo = -10000;
        caracteristicasFinales.at(i).varianza.minimo = 10000;
        caracteristicasFinales.at(i).varianza.sumatoria = 0;
        caracteristicasFinales.at(i).kurtois.maximo = -10000;
        caracteristicasFinales.at(i).kurtois.minimo = 10000;
        caracteristicasFinales.at(i).kurtois.sumatoria = 0;
        caracteristicasFinales.at(i).skewness.maximo = -10000;
        caracteristicasFinales.at(i).skewness.minimo = 10000;
        caracteristicasFinales.at(i).skewness.sumatoria = 0;
        caracteristicasFinales.at(i).rms.maximo = -10000;
        caracteristicasFinales.at(i).rms.minimo = 10000;
        caracteristicasFinales.at(i).rms.sumatoria = 0;
        const int totalelementos=caracteristicas.at(i).size();
        for(size_t j = 0; j < caracteristicas.at(i).size(); j++){
            if(caracteristicas.at(i).at(j).media > caracteristicasFinales.at(i).media.maximo) caracteristicasFinales.at(i).media.maximo = caracteristicas.at(i).at(j).media;
            if(caracteristicas.at(i).at(j).media < caracteristicasFinales.at(i).media.minimo) caracteristicasFinales.at(i).media.minimo = caracteristicas.at(i).at(j).media;
            caracteristicasFinales.at(i).media.sumatoria+=caracteristicas.at(i).at(j).media;
            if(caracteristicas.at(i).at(j).desviacionestandar > caracteristicasFinales.at(i).desviacionestandar.maximo) caracteristicasFinales.at(i).desviacionestandar.maximo = caracteristicas.at(i).at(j).desviacionestandar;
            if(caracteristicas.at(i).at(j).desviacionestandar < caracteristicasFinales.at(i).desviacionestandar.minimo) caracteristicasFinales.at(i).desviacionestandar.minimo = caracteristicas.at(i).at(j).desviacionestandar;
            caracteristicasFinales.at(i).desviacionestandar.sumatoria+=caracteristicas.at(i).at(j).desviacionestandar;
            if(caracteristicas.at(i).at(j).varianza > caracteristicasFinales.at(i).varianza.maximo) caracteristicasFinales.at(i).varianza.maximo = caracteristicas.at(i).at(j).varianza;
            if(caracteristicas.at(i).at(j).varianza < caracteristicasFinales.at(i).varianza.minimo) caracteristicasFinales.at(i).varianza.minimo = caracteristicas.at(i).at(j).varianza;
            caracteristicasFinales.at(i).varianza.sumatoria+=caracteristicas.at(i).at(j).varianza;
            if(caracteristicas.at(i).at(j).kurtois > caracteristicasFinales.at(i).kurtois.maximo) caracteristicasFinales.at(i).kurtois.maximo = caracteristicas.at(i).at(j).kurtois;
            if(caracteristicas.at(i).at(j).kurtois < caracteristicasFinales.at(i).kurtois.minimo) caracteristicasFinales.at(i).kurtois.minimo = caracteristicas.at(i).at(j).kurtois;
            caracteristicasFinales.at(i).kurtois.sumatoria+=caracteristicas.at(i).at(j).kurtois;
            if(caracteristicas.at(i).at(j).skewness > caracteristicasFinales.at(i).skewness.maximo) caracteristicasFinales.at(i).skewness.maximo = caracteristicas.at(i).at(j).skewness;
            if(caracteristicas.at(i).at(j).skewness < caracteristicasFinales.at(i).skewness.minimo) caracteristicasFinales.at(i).skewness.minimo = caracteristicas.at(i).at(j).skewness;
            caracteristicasFinales.at(i).skewness.sumatoria+=caracteristicas.at(i).at(j).skewness;
            if(caracteristicas.at(i).at(j).rms > caracteristicasFinales.at(i).rms.maximo) caracteristicasFinales.at(i).rms.maximo = caracteristicas.at(i).at(j).rms;
            if(caracteristicas.at(i).at(j).rms < caracteristicasFinales.at(i).rms.minimo) caracteristicasFinales.at(i).rms.minimo = caracteristicas.at(i).at(j).rms;
            caracteristicasFinales.at(i).rms.sumatoria+=caracteristicas.at(i).at(j).rms;
        }
        cout << "************************ Clase " << i << "************************" << endl;
        cout << "(Media) Maximo: " << caracteristicasFinales.at(i).media.maximo << " Media: " << caracteristicasFinales.at(i).media.sumatoria/totalelementos << " Minimo: " << caracteristicasFinales.at(i).media.minimo << endl;
        cout << "(Desviacion Estandar) Maximo: " << caracteristicasFinales.at(i).desviacionestandar.maximo << " Media: " << caracteristicasFinales.at(i).desviacionestandar.sumatoria/totalelementos << " Minimo: " << caracteristicasFinales.at(i).desviacionestandar.minimo << endl;
        cout << "(Varianza) Maximo: " << caracteristicasFinales.at(i).varianza.maximo << " Media: " << caracteristicasFinales.at(i).varianza.sumatoria/totalelementos << " Minimo: " << caracteristicasFinales.at(i).varianza.minimo << endl;
        cout << "(Kurtois) Maximo: " << caracteristicasFinales.at(i).kurtois.maximo << " Media: " << caracteristicasFinales.at(i).kurtois.sumatoria/totalelementos << " Minimo: " << caracteristicasFinales.at(i).kurtois.minimo << endl;
        cout << "(Skewness) Maximo: " << caracteristicasFinales.at(i).skewness.maximo << " Media: " << caracteristicasFinales.at(i).skewness.sumatoria/totalelementos << " Minimo: " << caracteristicasFinales.at(i).skewness.minimo << endl;
        cout << "(RMS) Maximo: " << caracteristicasFinales.at(i).rms.maximo << " Media: " << caracteristicasFinales.at(i).rms.sumatoria/totalelementos << " Minimo: " << caracteristicasFinales.at(i).rms.minimo << endl;

        cout << "**********************************************************" << endl;

    }
    for(size_t i = 0; i < caracteristicas.size(); i++){
        int totalrandom = 0;
        int totalmuestras = caracteristicas.at(i).size();
        vector<int> vectorandoms;
        while(totalrandom < caracteristicas.at(i).size()/5){
            vectorandoms.push_back(rand() % totalmuestras);
            totalrandom+=1;
            totalmuestras-=1;
        }
        for(size_t j = 0; j < vectorandoms.size(); j++){
            caracteristicasTestSet.at(i).push_back(caracteristicas.at(i).at(vectorandoms.at(j)));
            caracteristicas.at(i).erase(caracteristicas.at(i).begin() + vectorandoms.at(j));
        }
    }
    obtenerDatasets("train_" + nombresalida, caracteristicas);
    obtenerDatasets("test" + nombresalida, caracteristicasTestSet);
    return 0;
}
