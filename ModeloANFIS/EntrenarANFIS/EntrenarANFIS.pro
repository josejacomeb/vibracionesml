TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lboost_system -lboost_filesystem

INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu


unix:!macx: LIBS += -L$$PWD/../../../../../Downloads/fuzzylite/fuzzylite/release/bin/ -lfuzzylite-static

INCLUDEPATH += $$PWD/../../../../../Downloads/fuzzylite/fuzzylite/fl
DEPENDPATH += $$PWD/../../../../../Downloads/fuzzylite/fuzzylite/fl

unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../../Downloads/fuzzylite/fuzzylite/release/bin/libfuzzylite-static.a


unix:!macx: LIBS += -L$$PWD/../../../../../Downloads/fuzzylitex/build/ -lfuzzylitex

INCLUDEPATH += $$PWD/../../../../../Downloads/fuzzylitex/include
DEPENDPATH += $$PWD/../../../../../Downloads/fuzzylitex/include
