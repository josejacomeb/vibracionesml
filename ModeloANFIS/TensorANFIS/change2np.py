#!/usr/bin/python3
# -*- coding: utf-8 -*-
import time
import numpy as np
import argparse
import os
import random

def get_args():
    '''
    Gets the arguments from the command line.
    '''
    parser = argparse.ArgumentParser("Ejecuta el entrenador de ANFIS")
    # -- Create the descriptions for the commands
    train_desc = "Ruta al archivo de entrenamiento"
    test_desc = "Ruta al archivo de prueba"
    rules_desc = "Numero de reglas"
    inputs_desc = "Numero de entradas de Anfis"
    alpha_desc = "Tase de aprendizaje alfa"
    # -- Create the arguments
    parser.add_argument("--train", help=train_desc, default="./../train.dat")
    parser.add_argument("--test", help=test_desc, default='./../test.dat')
    parser.add_argument("--entradas", help=inputs_desc, type=int, default=6)
    parser.add_argument("--reglas", help=rules_desc, type=int, default=96)
    parser.add_argument("--alfa", help=alpha_desc, type=float, default=0.01)
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    readtrnData = []
    readtrnLbls = []
    trnData = np.empty(shape=(0, args.entradas))
    trnLbls = np.empty(shape=(0, 1))
    chkData = np.empty(shape=(0, args.entradas))
    chkLbls = np.empty(shape=(0, 1))
    f = open(args.train, "r")
    for x in f:
        lista = x.rstrip().split(" ")
        lista = [float(i) for i in lista]
        readtrnData.append(lista[:args.entradas])
        readtrnLbls.append(int(lista[args.entradas]))
    f.close()
    print("Empieza a randomizar")
    while len(readtrnData) > 0:
        numero_aleatorio = int(random.random()*len(readtrnData))
        trnData = np.vstack((trnData, np.array(readtrnData[numero_aleatorio])))
        trnLbls = np.vstack((trnLbls, np.array(readtrnLbls[numero_aleatorio])))
        readtrnData.pop(numero_aleatorio)
        readtrnLbls.pop(numero_aleatorio)
    f = open(args.test, "r")
    for x in f:
        lista = x.rstrip().split(" ")
        lista = [float(i) for i in lista]
        chkData = np.vstack((chkData, np.array(lista[:args.entradas])))
        chkLbls = np.vstack((chkLbls, np.array(lista[args.entradas])))
    f.close()
    print(trnData.shape)
    print(trnLbls.shape)
    print(chkData.shape)
    print(chkLbls.shape)

if __name__ == '__main__':
    main()
