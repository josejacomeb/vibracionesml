#!/usr/bin/python3
# -*- coding: utf-8 -*-
import time
import os
import logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #No mostrar errores
logging.getLogger('tensorflow').setLevel(logging.FATAL) #Eliminar los mensajes de errores
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from anfis import ANFIS
import argparse
trained_checkpoint_prefix = 'ModeloEntrenado/my-model/'
import sys
def get_args():
    '''
    Gets the arguments from the command line.
    '''
    parser = argparse.ArgumentParser("Ejecuta el entrenador de ANFIS")
    # -- Create the descriptions for the commands
    train_desc = "Ruta al archivo de entrenamiento"
    test_desc = "Ruta al archivo de prueba"
    rules_desc = "Numero de reglas"
    inputs_desc = "Numero de entradas de Anfis"
    alpha_desc = "Tase de aprendizaje alfa"
    de_desc = "Valor de la Desviacion estandar calculado"
    ku_desc = "Valor de kurtois calculado"
    me_desc = "Valor de la media calculado"
    rms_desc = "Valor RMS calculado"
    sk_desc = "Valor de skewness calculado"
    va_desc = "Valor de la varianza calculado"

    # -- Create the arguments
    parser.add_argument("--de", help=de_desc, required=True, type=float)
    parser.add_argument("--ku", help=ku_desc, required=True, type=float)
    parser.add_argument("--me", help=me_desc, required=True, type=float)
    parser.add_argument("--rms", help=rms_desc, required=True, type=float)
    parser.add_argument("--sk", help=sk_desc, required=True, type=float)
    parser.add_argument("--va", help=va_desc, required=True, type=float)
    parser.add_argument("--test", help=test_desc, default='./../test1.dat')
    parser.add_argument("--entradas", help=inputs_desc, type=int, default=6)
    parser.add_argument("--reglas", help=rules_desc, type=int, default=96)
    parser.add_argument("--alfa", help=alpha_desc, type=float, default=0.01)
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    chkData = []
    chkData.append([args.de, args.ku, args.me, args.rms, args.sk, args.va])
    # Training
    fis = ANFIS(n_inputs=args.entradas, n_rules=args.reglas, learning_rate=args.alfa)
    nombrearchivo = "MCSA_ANFIS"
    with tf.compat.v1.Session() as sess:
        saver = tf.compat.v1.train.Saver()
        # Restore from checkpoint
        saver = tf.compat.v1.train.import_meta_graph(nombrearchivo+'.meta')
        #sess.run(fis.init_variables)
        saver.restore(sess, "./"+nombrearchivo)
        val_pred = fis.infer(sess, chkData)
        #print(val_pred)
        #print("Val Pred")
        print('{'+ str(abs(int(round(val_pred[0])))) + '}')
        sys.stdout.flush()



if __name__ == '__main__':
    main()
