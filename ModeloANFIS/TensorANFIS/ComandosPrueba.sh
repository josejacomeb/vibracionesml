#!/bin/bash
#Test
de=$1
ku=$2
me=$3
rms=$4
sk=$5
va=$6
clase=$7
reglas=$8
vacio=""
if [ "$reglas" = "$vacio" ]; then
    reglas=96
fi
echo "===========Calculo Clase==========="
python3 -W ignore MCSAAnfisTest.py --de ${de} --ku ${ku} --me ${me} --rms ${rms} --sk ${sk} --va ${va} --reglas ${reglas}
echo "===========Clase real: ${clase}==========="
