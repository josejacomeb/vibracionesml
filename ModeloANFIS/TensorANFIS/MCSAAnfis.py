#!/usr/bin/python3
# -*- coding: utf-8 -*-
import time
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from anfis import ANFIS
import argparse
import os
import random

def get_args():
    '''
    Gets the arguments from the command line.
    '''
    parser = argparse.ArgumentParser("Ejecuta el entrenador de ANFIS")
    # -- Create the descriptions for the commands
    train_desc = "Ruta al archivo de entrenamiento"
    test_desc = "Ruta al archivo de prueba"
    rules_desc = "Numero de reglas"
    inputs_desc = "Numero de entradas de Anfis"
    alpha_desc = "Tasa de aprendizaje alfa"
    iter_desc = "Numero de iteraciones"
    # -- Create the arguments
    parser.add_argument("--train", help=train_desc, default="./../train.dat")
    parser.add_argument("--test", help=test_desc, default='./../test.dat')
    parser.add_argument("--entradas", help=inputs_desc, type=int, default=6)
    parser.add_argument("--reglas", help=rules_desc, type=int, default=96)
    parser.add_argument("--alfa", help=alpha_desc, type=float, default=0.01)
    parser.add_argument("--iter", help=iter_desc, type=int, default=150)
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    readtrnData = []
    readtrnLbls = []
    trnData = []
    trnLbls = []
    chkData = []
    chkLbls = []
    f = open(args.train, "r")
    for x in f:
        lista = x.rstrip().split(" ")
        lista = [float(i) for i in lista]
        readtrnData.append(lista[:args.entradas])
        readtrnLbls.append(int(lista[len(lista)-1]))
    f.close()
    print("Empieza a randomizar")
    while len(readtrnData) > 0:
        numero_aleatorio = int(random.random()*len(readtrnData))
        trnData.append(readtrnData[numero_aleatorio])
        trnLbls.append(readtrnLbls[numero_aleatorio])
        readtrnData.pop(numero_aleatorio)
        readtrnLbls.pop(numero_aleatorio)
    print("Salio")
    f = open(args.test, "r")
    for x in f:
        lista = x.rstrip().split(" ")
        lista = [float(i) for i in lista]
        chkData.append(lista[:args.entradas])
        chkLbls.append(lista[len(lista) - 1])
    f.close()

    # Training
    print("Empieza a entrenar")
    nombrearchivo = 'MCSA_ANFIS'
    num_epochs = args.iter
    fis = ANFIS(n_inputs=args.entradas, n_rules=args.reglas, learning_rate=args.alfa)
    with tf.compat.v1.Session() as sess:
        sess.run(fis.init_variables)
        saver = tf.train.Saver()#, v3: "y/Assign", v4: "beta1_power/Assign"})
        trn_costs = []
        val_costs = []
        time_start = time.time()
        for epoch in range(num_epochs):
            #  Run an update step
            trn_loss, trn_pred = fis.train(sess, trnData, trnLbls)
            # Evaluate on validation set
            val_pred, val_loss = fis.infer(sess, chkData, chkLbls)
            if epoch % 10 == 0:
                print("Train cost after epoch %i: %f" % (epoch, trn_loss))
                #print("Weight matrix: {1}".format(sess.run(variables[1])))
                #print("Weight matrix: {2}".format(sess.run(variables[2])))
            if epoch == num_epochs - 1:
                time_end = time.time()
                print("Elapsed time: %f" % (time_end - time_start))
                print("Validation loss: %f" % val_loss)
                # Plot real vs. predicted
                pred = np.vstack((np.expand_dims(trn_pred, 1), np.expand_dims(val_pred, 1)))
                plt.figure(1)
                plt.plot(pred)
            trn_costs.append(trn_loss)
            val_costs.append(val_loss)
        saver.save(sess, nombrearchivo)
        #saver.save(sess, export_dir)
         # Export checkpoint to SavedModel
        #builder = tf.compat.v1.saved_model.builder.SavedModelBuilder(export_dir)
        #builder.add_meta_graph_and_variables(sess, ["train", "serve"], strip_default_attrs=True)
        #builder.save()



        plt.figure(2)
        plt.subplot(2, 1, 1)
        plt.plot(np.squeeze(trn_costs))
        plt.title("Training loss, Learning rate =" + str(args.alfa))
        plt.subplot(2, 1, 2)
        plt.plot(np.squeeze(val_costs))
        plt.title("Validation loss, Learning rate =" + str(args.alfa))
        plt.ylabel('Cost')
        plt.xlabel('Epochs')
        # Plot resulting membership functions
        #fis.plotmfs(sess)
        plt.show()

if __name__ == '__main__':
    main()
